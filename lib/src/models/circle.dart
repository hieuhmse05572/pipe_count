import 'package:flutter/cupertino.dart';

class Circle {
  int index;
  int type;
  Offset pos;
  double confidence;
  double radius;
  bool isSelected;

  Circle(
      int type, Offset pos, double confidence, double radius, bool isSelected) {
    this.type = type;
    this.pos = pos;
    this.confidence = confidence;
    this.radius = radius;
    this.isSelected = isSelected;
  }
  factory Circle.fromJson(Map<String, dynamic> json) {
    double dx = json["dx"] as double;
    double dy = json["dy"] as double;

    return Circle(json["type"] as int, Offset(dx, dy), json["confidence"],
        json["radius"], false);
  }
}
