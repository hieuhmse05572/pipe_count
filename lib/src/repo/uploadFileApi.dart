import 'dart:async';

import 'package:http/http.dart';
import 'package:pipeCount/src/utils/NetworkUtil.dart';
import 'dart:ui' as ui;

class UploadFileApi {

  Future<String> uploadImage(filename) async {
    try {

    String url = NetworkUtil.BASE_URL + "/api/file/upload";
    var request = MultipartRequest('POST', Uri.parse(url));
    request.files.add(await MultipartFile.fromPath('file', filename));
    request.headers.addAll(NetworkUtil.getRequestHeaders());

      var res = await request.send().timeout(const Duration(seconds: 40));
      String str = await res.stream.bytesToString();
      return str;
      // Remaining code
    } on Exception catch (e) {
      print(e);
      return "timeout";
    }

  }
}
