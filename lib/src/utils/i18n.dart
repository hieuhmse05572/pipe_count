import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

String text(BuildContext context, String key) {
  String txt = FlutterI18n.translate(context, key);
  return txt;
}
