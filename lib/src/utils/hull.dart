import 'package:flutter/cupertino.dart';
import 'dart:math';

class Hull {
  // To find orientation of ordered triplet (p, q, r).
  // The function returns following values
  // 0 --> p, q and r are colinear
  // 1 --> Clockwise
  // 2 --> Counterclockwise
  static orientation(Offset p, Offset q, Offset r) {
    double val = (q.dy - p.dy) * (r.dx - q.dx) - (q.dx - p.dx) * (r.dy - q.dy);

    if (val == 0) return 0; // collinear
    return (val > 0) ? 1 : 2; // clock or counterclock wise
  }

  // Prints convex hull of a set of n points.
  static convexHull(List<Offset> points, int n) {
    // There must be at least 3 points
    if (n < 3) return;

    // Initialize Result
    List<Offset> hull = new List<Offset>();

    // Find the leftmost point
    int l = 0;

    for (int i = 1; i < n; i++) {
      if (points[i].dx < points[l].dx) l = i;
    }
    // Start from leftmost point, keep moving
    // counterclockwise until reach the start point
    // again. This loop runs O(h) times where h is
    // number of points in result or output.
    int p = l, q;
    do {
      // Add current point to result
      hull.add(points[p]);

      // Search for a point 'q' such that
      // orientation(p, x, q) is counterclockwise
      // for all points 'x'. The idea is to keep
      // track of last visited most counterclock-
      // wise point in q. If any point 'i' is more
      // counterclock-wise than q, then update q.
      q = (p + 1) % n;

      for (int i = 0; i < n; i++) {
        // If i is more counterclockwise than
        // current q, then update q
        if (orientation(points[p], points[i], points[q]) == 2) q = i;
      }

      // Now q is the most counterclockwise with
      // respect to p. Set p as q for next iteration,
      // so that q is added to result 'hull'
      p = q;
    } while (p != l); // While we don't come to first
    // point

    // Print Result
    return hull;
  }

  // The function that returns true if
  // line segment 'p1q1' and 'p2q2' intersect.
  static bool doIntersect(Offset p1, Offset q1, Offset p2, Offset q2) {
    // Find the four orientations needed for
    // general and special cases
    int o1 = orientation(p1, q1, p2);
    int o2 = orientation(p1, q1, q2);
    int o3 = orientation(p2, q2, p1);
    int o4 = orientation(p2, q2, q1);

    // General case
    if (o1 != o2 && o3 != o4) {
      return true;
    }

    // Special Cases
    // p1, q1 and p2 are colinear and
    // p2 lies on segment p1q1
    if (o1 == 0 && onSegment(p1, p2, q1)) {
      return true;
    }

    // p1, q1 and p2 are colinear and
    // q2 lies on segment p1q1
    if (o2 == 0 && onSegment(p1, q2, q1)) {
      return true;
    }

    // p2, q2 and p1 are colinear and
    // p1 lies on segment p2q2
    if (o3 == 0 && onSegment(p2, p1, q2)) {
      return true;
    }

    // p2, q2 and q1 are colinear and
    // q1 lies on segment p2q2
    if (o4 == 0 && onSegment(p2, q1, q2)) {
      return true;
    }

    // Doesn't fall in any of the above cases
    return false;
  }

  static bool onSegment(Offset p, Offset q, Offset r) {
    if (q.dx <= [p.dx, r.dx].reduce(max) &&
        q.dx > [p.dx, r.dx].reduce(min) &&
        q.dy <= [p.dy, r.dy].reduce(max) &&
        q.dy >= [p.dy, r.dy].reduce(min)) {
      return true;
    }
    return false;
  }

  static bool isInside(List<Offset> polygon, Offset p) {
    // There must be at least 3 vertices in polygon[]
    int n = polygon.length;
    if (n < 3) {
      return false;
    }

    // Create a point for line segment from p to infinite
    Offset extreme = new Offset(10000, p.dy);

    // Count intersections of the above line
    // with sides of polygon
    int count = 0, i = 0;
    do {
      int next = (i + 1) % n;

      // Check if the line segment from 'p' to
      // 'extreme' intersects with the line
      // segment from 'polygon[i]' to 'polygon[next]'
      if (doIntersect(polygon[i], polygon[next], p, extreme)) {
        // If the point 'p' is colinear with line
        // segment 'i-next', then check if it lies
        // on segment. If it lies, return true, otherwise false
        if (orientation(polygon[i], p, polygon[next]) == 0) {
          return onSegment(polygon[i], p, polygon[next]);
        }

        count++;
      }
      i = next;
    } while (i != 0);

    // Return true if count is odd, false otherwise
    return (count % 2 == 1); // Same as (count%2 == 1)
  }
}
