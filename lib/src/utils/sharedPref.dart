import 'package:shared_preferences/shared_preferences.dart';

class SharePref {
  static SharedPreferences prefs;
  static final SharePref _inst = SharePref._internal();
  SharePref._internal();

  static init() async {
    prefs = await SharedPreferences.getInstance();
  }

  factory SharePref() {
    return _inst;
  }

  static updateSwitch(String text){
    bool value = prefs.getBool(text);
    if(value != null){
      return !value;
    }else{
       prefs.setBool(text, false);
       return false;
    }
  }
}
