import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pipeCount/src/helper/uploadFileHelper.dart';
import 'package:pipeCount/src/models/circle.dart';
import 'package:pipeCount/src/utils/a.dart';
import 'package:pipeCount/src/utils/hull.dart';
import 'package:pipeCount/src/utils/sharedPref.dart';
import 'dart:ui' as ui;

import 'package:pipeCount/ui/screen/count/ImageEditor.dart';

class MenuProvider with ChangeNotifier {
  List<Offset> points = List();
  List<Offset> hull = List();
  List<Circle> tags = List();
  GlobalKey screen = new GlobalKey();
  Size size;
  String imagePath = "";
  ImageSaver imageSaver;
  final _currentMainTab = StreamController<int>.broadcast();
  Stream<int> get currentMainTab => _currentMainTab.stream;
  final _currentSubTab = StreamController<int>.broadcast();
  Stream<int> get currentSubTab => _currentSubTab.stream;

  final _current = StreamController<String>.broadcast();
  Stream<String> get current => _current.stream;

  final _action = StreamController<String>.broadcast();
  Stream<String> get action => _action.stream;

  final _setting = StreamController<bool>.broadcast();
  Stream<bool> get setting => _setting.stream;

  final _sizeOfTag = StreamController<double>.broadcast();
  Stream<double> get sizeOfTag => _sizeOfTag.stream;

  final _bottomTab = StreamController<String>.broadcast();
  Stream<String> get bottomTab => _bottomTab.stream;

  final _tagsStream = StreamController<List<Circle>>.broadcast();
  Stream<List<Circle>> get tagsStream => _tagsStream.stream;

  final _result = StreamController<int>.broadcast();
  Stream<int> get result => _result.stream;

  String mainTab = "";
  String subTab = "";
  String currentStr = "";
  double tagSize = 50;
  double tagConfidence = 70;
  String actionStr = "";
  bool isShowNumber = true;
  int totalTag = 0;
  bool isSaveOriginalImage = false;

  Color background =  Colors.blue.withOpacity(0.7);
  Color color = Colors.white;

  changeSetting(bool status) {
    _setting.add(status);
  }

  MenuProvider(){
    print('init app');

  }

  // Future<String> ScreenShota() async {
  //   if (await Permission.contacts.request().isGranted) {
  //   }
  //
  //   Map<Permission, PermissionStatus> statuses = await [
  //     Permission.storage,
  //   ].request();
  //   RenderRepaintBoundary boundary = screen.currentContext.findRenderObject();
  //   ui.Image image = await boundary.toImage();
  //   ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
  //   // var file= await writeToFile(byteData);
  //   // print(file.path);
  //   final result = await ImageGallerySaver.saveImage(
  //       byteData.buffer.asUint8List(),
  //       quality: 60);
  //   print(result['filePath']);
  //   return result['filePath'];
  // }
  Future<ui.Image> getImage() {
    ui.PictureRecorder recorder = ui.PictureRecorder();
    Canvas canvas = Canvas(recorder);
    Size sizee = Size(size.width, size.height + 100);
    imageSaver.paint(canvas, sizee);
    return recorder
        .endRecording()
        .toImage(size.width.floor(), size.height.floor() + 100);
  }

  Future<bool> saveOriginalImage(String fileName) async {
    final data = await Util.readFileByte(imagePath);
    await ImageGallerySaver.saveImage(
        data.buffer.asUint8List(),
        name: "${fileName}_original",
        quality: 80);
    return true;
  }

  Future<String> ScreenShot() async {
    String fileName = "${new DateTime.now().millisecondsSinceEpoch}";
    if(isSaveOriginalImage) saveOriginalImage(fileName);

    ui.Image imagee = await getImage();
    ByteData byteData =
    await imagee.toByteData(format: ui.ImageByteFormat.png);
    final result = await ImageGallerySaver.saveImage(
    byteData.buffer.asUint8List(),
    name: "${fileName}_${totalTag}",
    quality: 80);
    print(result['filePath']);
    return result['filePath'];
  }

  reset() {
    points = List();
    hull = List();
    tags = List();
  }

  updateResult(){
    totalTag = getTagInnerArea(tags).length;
    _result.add(totalTag);
  }

  List<Circle> getTagInnerArea(List<Circle> tags) {
    if (hull.length == 0) return tags;
    List<Circle> result = List();
    for (int i = 0; i < tags.length; i++) {
      if (Hull.isInside(hull, Offset(tags[i].pos.dx, tags[i].pos.dy))) {
        if (tags[i].confidence *100 >= tagConfidence) {
          result.add(tags[i]);
        }
      }
    }
    return result;
  }

  Future<bool> getResultCouting() async {
    tags = [];
    List<Circle> circles = await UploadFileHelper().uploadFile(imagePath);
    if(circles == null) return false;
    tags.addAll(circles);
    _tagsStream.add(tags);
    refresh();
    return true;
  }



  addTag(Circle circle) {
    _tagsStream.add(tags);
  }

  removeTag(List<Circle> tags) {
    this.tags = tags;
    _tagsStream.add(tags);
  }

  clearArea() {
    points = List();
    hull = List();
    refresh();
  }

  refresh() {
    _current.add(currentStr);
  }

  languageRefresh() {
    _bottomTab.add("");
    _currentSubTab.add(int.parse(subTab));
    _currentMainTab.add(int.parse(mainTab));
    // updateResult();
  }

  changeMainTab(int index) {
    _currentMainTab.add(index);
    mainTab = index.toString();
    print(index);
    changeAction("cancel");
    changeSubTab(-1);
  }

  changeSubTab(int index) {
    _currentSubTab.add(index);
    subTab = index.toString();
    currentStr = mainTab + subTab;
    _current.add(currentStr);
    print("sub ${index}");
  }

  changeAction(String action) {
    actionStr = action;
    _action.add(action);
  }

  changeSizeOfTags(double size) {
    tagSize = size;
    // _sizeOfTag.add(size);
  }
  changeConficenceOfTags(double conf) {
    print(conf);
    tagConfidence = conf;
    // _sizeOfTag.add(size);
  }

  // set currentMainTab(value) => this.currentMainTab.value = value;
  // set currentSubTab(value) => this.currentSubTab.value = value;

  // changeSubTab(int mainTabIndex, int subTabIndex) {
  // if (subTabIndex == currentSubTab) {
  //   currentSubTab = -1;
  // } else {
  //   currentSubTab = subTabIndex;
  //   currentMainTab = mainTabIndex;
  // }
  // }
}
