import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ImageController extends GetxController {
  final _isImageloaded = false.obs;
  List<Offset> points = List();

  ImageController() {
    points = List<Offset>();
  }

  get isImageloaded => this._isImageloaded.value;

  void setIsImageloaded(bool bool) {
    this._isImageloaded.value = bool;
  }

  void onScaleEnd(detail) {}
  void onScaleStart(ScaleStartDetails scaleStartDetails) {}

  void onScaleUpdate(ScaleUpdateDetails scaleUpdateDetails) {
    this.points.add(scaleUpdateDetails.localFocalPoint);
    update();
  }
}
