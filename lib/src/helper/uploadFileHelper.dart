import 'dart:async';
import 'dart:convert';

import 'package:pipeCount/src/models/circle.dart';
import 'package:pipeCount/src/repo/LoginApi.dart';
import 'package:pipeCount/src/repo/uploadFileApi.dart';
import 'package:pipeCount/src/utils/NetworkUtil.dart';

class UploadFileHelper {
  final api = UploadFileApi();

  Future<List<Circle>> uploadFile(String imagePath) async {
    print(imagePath);
    try {
      final apiResult = await api.uploadImage(imagePath);
      if (apiResult == "timeout") return null;

      final data = jsonDecode(apiResult);
      if (data != null) {
        List<Circle> circles = List();
        for (Map i in data) {
          Circle circle = Circle.fromJson(i);
          circles.add(circle);
        }
        return circles;
      }
    } on Exception catch (e) {
      print("e");
      return null;
    }
    return null;
  }
}
