import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:pipeCount/getIt.dart';
import 'package:pipeCount/src/controllers/counter_screen_controller.dart';
import 'package:pipeCount/src/utils/i18n.dart';

class BottomTab extends StatefulWidget {
  @override
  _BottomTabState createState() => _BottomTabState();
}

class _BottomTabState extends State<BottomTab>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  final menuProvider = getIt<MenuProvider>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(length: 3, vsync: this);
    tabController.index = 0;
    menuProvider.changeMainTab(0);
    menuProvider.changeSubTab(-1);
  }

  @override
  Widget build(BuildContext context) {
    return _getbuildBottomTabStream(context);
  }

  Widget buildBottomTab() {
    return Container(
      height: 65,
      color: Colors.black,
      width: Size.infinite.width,
      child: Center(
        child: TabBar(
          controller: tabController,
          onTap: (index) {
            menuProvider.changeMainTab(index);
            menuProvider.changeSubTab(-1);
          },
          indicator: UnderlineTabIndicator(
            borderSide: BorderSide(width: 1, color: Colors.pink),
            insets: EdgeInsets.symmetric(horizontal: 20),
          ),
          // indicatorColor: Colors.blueAccent,
          unselectedLabelColor: Colors.white,
          labelColor: Colors.pink,
          isScrollable: true,
          tabs: <Widget>[
            // buildTab("Area", 14),
            // buildTab("Count", 20),
            // buildTab("Edit", 14),
            buildTab(text(context, "button.area"), 14),
            buildTab(text(context, "button.count"), 20),
            buildTab(text(context, "button.edit"), 14),
            // buildTab('Hide number'),
          ],
        ),
      ),
    );
  }

  Widget _getbuildBottomTabStream(BuildContext context) {
    return StreamBuilder<String>(
        // initialData: "",
        stream: menuProvider.bottomTab,
        builder: (context, snapshot) {
          return buildBottomTab();
        });
  }

  Widget buildTab(String title, double size) {
    return Container(
      padding: EdgeInsets.only(left: 3, right: 3, bottom: 5),
      child: Text(
        '$title',
        style: TextStyle(fontSize: size),
      ),
    );
  }
}
