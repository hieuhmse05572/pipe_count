import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pipeCount/src/utils/i18n.dart';

Widget innerLoading() {
  return Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CupertinoActivityIndicator(
          radius: 14,
        ),
        SizedBox(
          height: 6,
          width: 1,
        ),
        Container(alignment: Alignment.center, child: Text('ローディング...'))
      ],
    ),
  );
}

dialogMessage(BuildContext context, String title,  String msg) {
  return Dialog(
//     contentPadding: EdgeInsets.all(5),
//     elevation: 0.0,
//     backgroundColor: Colors.transparent,

    child: Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      // width: Size.infinite.width,
      // height: 134,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                title,
                // 'Counting...',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
              InkWell(
                child: Icon(
                  Icons.clear,
                  color: Colors.purple,
                ),
                onTap: () {
                  Navigator.pop(context, "0");
                },
              )
            ],
          ),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                // "Please wait !!!",
               msg,
                style: TextStyle(fontSize: 15),
              ),
            ],
          ),
          SizedBox(
            height: 10,
            width: 1,
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 7),
            child: LinearProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.pink),
              backgroundColor: Colors.pink.withOpacity(0.4),
              // valueColor: Colors.pink,
            ),
          )
        ],
      ),
    ),
  );
}

dialogSetting(BuildContext context, String msg) {
  return Dialog(
//     contentPadding: EdgeInsets.all(5),
//     elevation: 0.0,
//     backgroundColor: Colors.transparent,

    child: Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      // width: Size.infinite.width,
      // height: 134,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Counting...',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
              InkWell(
                child: Icon(
                  Icons.clear,
                  color: Colors.purple,
                ),
                onTap: () {
                  Navigator.pop(context, "0");
                },
              )
            ],
          ),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                "Please wait !!!",
                style: TextStyle(fontSize: 15),
              ),
            ],
          ),
          SizedBox(
            height: 10,
            width: 1,
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 7),
            child: LinearProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.pink),
              backgroundColor: Colors.pink.withOpacity(0.4),
              // valueColor: Colors.pink,
            ),
          )
        ],
      ),
    ),
  );
}

Widget Loading() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Center(
        child: Container(
            height: 100,
            width: 100,
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(7)),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    blurRadius: 4,
                    offset: Offset(1, 1))
              ],
              color: Colors.grey,
            ),
            margin: EdgeInsets.all(20),
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CupertinoActivityIndicator(
                  radius: 14,
                ),
                SizedBox(
                  height: 6,
                  width: 1,
                ),
                Text(
                  'Loading...',
                  style: TextStyle(color: Colors.white),
                )
              ],
            ))),
      ),
    ],
  );
}

Dialog dialogLoading() {
  return Dialog(
      elevation: 0, backgroundColor: Colors.transparent, child: Loading());
}
