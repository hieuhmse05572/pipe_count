import 'package:flutter/material.dart';

class DialogUtil {
  static Future<String> showDialogSettingLeft(
      BuildContext context, Widget child) async {
    return await showGeneralDialog(
      context: context,
      barrierColor: Colors.black12.withOpacity(0.6), // background color
      barrierDismissible:
          false, // should dialog be dismissed when tapped outside
      // barrierLabel: "Dialog", // label for barrier
      transitionBuilder: (ctx, animation, _, child) {
        return FractionalTranslation(
          translation: Offset(animation.value - 1, 0),
          child: child,
        );
      },
      transitionDuration: Duration(
          milliseconds:
              300), // how long it takes to popup dialog after button click
      pageBuilder: (context, __, ___) {
        // your widget implementation
        return child;
      },
    );
  }

  static Future<String> showDialogSettingRight(
      BuildContext context, Widget child) async {
    return await showGeneralDialog(
      context: context,
      barrierColor: Colors.black12.withOpacity(0.6), // background color
      barrierDismissible:
          false, // should dialog be dismissed when tapped outside
      // barrierLabel: "Dialog", // label for barrier
      transitionBuilder: (ctx, animation, _, child) {
        return FractionalTranslation(
          translation: Offset(1 - animation.value, 0),
          child: child,
        );
      },
      transitionDuration: Duration(
          milliseconds:
              300), // how long it takes to popup dialog after button click
      pageBuilder: (context, __, ___) {
        // your widget implementation
        return child;
      },
    );
  }

  static Offset fromLeft(Animation animation) {
    return Offset(animation.value - 1, 0);
  }

  static Offset fromRight(Animation animation) {
    return Offset(1 - animation.value, 0);
  }
}
