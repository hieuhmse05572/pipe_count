import 'package:flutter/material.dart';

class Line extends StatelessWidget {
  Line({this.axis, this.height});
  Axis axis;
  double height;
  @override
  Widget build(BuildContext context) {
    return buildLine(axis);
  }

  Widget buildLine(Axis axis) {
    if (axis == Axis.horizontal)
      return Divider(
        thickness: 0.5,
        height: 1,
        color: Colors.white.withOpacity(0.5),
      );

    if (axis == Axis.vertical)
      return Container(
        height: height,
        child: VerticalDivider(
          thickness: 1,
          color: Colors.white.withOpacity(0.5),
        ),
      );
  }
}
