import 'package:flutter/material.dart';

class CustomSwitch extends StatefulWidget {
  bool initSwitchValue;
  Function onTap;
  CustomSwitch({this.initSwitchValue, this.onTap});
  @override
  __SwitchState createState() => __SwitchState();
}

class __SwitchState extends State<CustomSwitch> {
  bool switchValue;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    switchValue = widget.initSwitchValue;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          switchValue = !switchValue;
        });
        // widget.onTap(switchValue);
      },
      child: Switch(
        activeColor: Colors.pink,
        value: switchValue,
        onChanged: (value) {
          setState(() {
            switchValue = value;
          });
          widget.onTap(switchValue);
        },
      ),
    );
  }
}
