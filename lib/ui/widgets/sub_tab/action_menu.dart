import 'package:flutter/material.dart';
import 'package:pipeCount/getIt.dart';
import 'package:pipeCount/src/controllers/counter_screen_controller.dart';
import 'package:pipeCount/src/utils/i18n.dart';
import 'package:pipeCount/ui/screen/camera/animation.dart';
import 'package:pipeCount/ui/screen/camera/slider.dart';
import 'package:pipeCount/ui/widgets/line.dart';

class ActionMenu extends StatelessWidget {
  final menuProvider = getIt<MenuProvider>();

  @override
  Widget build(BuildContext context) {
    return _getStream(context);
  }

  Widget _getStream(BuildContext context) {
    return StreamBuilder<String>(
        initialData: "-1",
        stream: menuProvider.current,
        builder: (context, snapshot) {
          return subBar(context, snapshot.data);
        });
  }

  Widget subBar(BuildContext context, String current) {
    switch (current) {
      case "20":
        return Padding(
          padding: const EdgeInsets.only(bottom: 5.0),
          child: Container(
            color: Colors.black.withOpacity(0.4),
            height: 50,
            width: Size.infinite.width,
            child: SensitivitySlider(
              type: text(context, "button.sensitivity"),
            ),
          ),
        );
      case "21":
        return Padding(
          padding: const EdgeInsets.only(bottom: 5.0),
          child: Container(
            color: Colors.black.withOpacity(0.4),
            height: 50,
            width: Size.infinite.width,
            child: SizeSlider(
              type: text(context, "button.size"),
            ),
          ),
        );
      case "22":
        return _getIconStream(context);
      default:
        return Container();
    }
  }

  Widget _getIconStream(BuildContext context) {
    return StreamBuilder<String>(
        initialData: "",
        stream: menuProvider.action,
        builder: (context, snapshot) {
          return snapshot.data == "" ? getTags(context) : getMinTags();
        });
  }

  Widget getMinTags() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(
            child: Container(
              child: Text(' '),
            ),
          ),
          Expanded(
            child: Container(
              child: Text(' '),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.centerRight,
              padding: EdgeInsets.all(7),
              // color: Colors.black.withOpacity(0.4),
              child: AnimationList(
                children: [
                  buildIconButton(
                    icon: Icons.add_circle_outlined,
                    action: "add",
                  ),
                  SizedBox(
                    height: 7,
                    width: 1,
                  ),
                  buildIconButton(
                    icon: Icons.remove_circle_outlined,
                    action: "remove",
                  ),
                  SizedBox(
                    height: 7,
                    width: 1,
                  ),
                  buildIconButton(
                    icon: Icons.cancel,
                    action: "cancel",
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget getTags(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 5.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(
            child: Container(
              child: Text(' '),
            ),
          ),
          Expanded(
            child: Container(
              child: Text(' '),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.black.withOpacity(0.4),
              child: AnimationList(
                children: [
                  buildActionButton(
                    name: text(context, "button.add_tags"),
                    icon: Icons.add_circle_outlined,
                    action: "add",
                  ),
                  Line(
                    axis: Axis.horizontal,
                  ),
                  buildActionButton(
                    name: text(context, "button.remove_tags"),
                    icon: Icons.remove_circle_outlined,
                    action: "remove",
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class buildIconButton extends StatelessWidget {
  final menuProvider = getIt<MenuProvider>();
  String action;
  IconData icon;
  buildIconButton({this.icon, this.action});
  @override
  Widget build(BuildContext context) {
    Color color = menuProvider.actionStr == action ? Colors.pink : Colors.white;
    return InkWell(
      child: Icon(
        icon,
        size: 26,
        color: color,
      ),
      onTap: () {
        menuProvider.changeAction(action);
        if (action == "cancel") menuProvider.changeSubTab(-1);
      },
    );
  }
}

class buildActionButton extends StatelessWidget {
  final menuProvider = getIt<MenuProvider>();

  String name;
  IconData icon;
  String action;
  buildActionButton({this.name, this.icon, this.action});
  @override
  Widget build(BuildContext context) {
    return _buildTab();
  }

  Widget _buildTab() {
    return Container(
      height: 40,
      child: RaisedButton(
        padding: EdgeInsets.zero,
        elevation: 0,
        splashColor: Colors.pink.withOpacity(0.5),
        color: Colors.transparent,
        onPressed: () {
          menuProvider.changeAction(action);
        },
        child: Container(
          height: 40,
          child: Row(
            children: [
              Text('  '),
              Icon(
                icon,
                color: Colors.white,
              ),
              Text(
                ' ${name}',
                style: TextStyle(color: Colors.white, fontSize: 12),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
