import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:pipeCount/getIt.dart';
import 'package:pipeCount/src/controllers/counter_screen_controller.dart';
import 'package:pipeCount/src/utils/i18n.dart';
import 'package:pipeCount/ui/widgets/line.dart';

class TagsMenu extends StatelessWidget {
  final menuProvider = getIt<MenuProvider>();

  @override
  Widget build(BuildContext context) {
    return _areaWidgetAnimation(context);
  }

  Widget _areaWidgetAnimation(BuildContext context) {
    // return currentTab == 0 ? _areaWidget() : Container();
    return
        // hasClick
        //   ? slider()
        //   :
        Container(
      // width: 100,
      width: Size.infinite.width,
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        color: Colors.black.withOpacity(0.4),
      ),
      child: SingleChildScrollView(
        child: AnimationLimiter(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.min,
            children: AnimationConfiguration.toStaggeredList(
              duration: const Duration(milliseconds: 100),
              childAnimationBuilder: (widget) => SlideAnimation(
                horizontalOffset: 40.0,
                child: FadeInAnimation(
                  child: widget,
                ),
              ),
              children: [
                Tab(
                  name: text(context, "button.sensitivity"),
                  icon: Icons.grain,
                  index: 0,
                  onTap: () {
                    menuProvider.changeSubTab(0);
                  },
                ),
                Line(
                  axis: Axis.vertical,
                  height: 35,
                ),
                Tab(
                  name: text(context, "button.size"),
                  icon: Icons.hdr_strong_rounded,
                  index: 1,
                  onTap: () {
                    menuProvider.changeSubTab(1);
                  },
                ),
                Line(
                  axis: Axis.vertical,
                  height: 35,
                ),
                Tab(
                  name: text(context, "button.tags"),
                  icon: Icons.gps_not_fixed,
                  index: 2,
                  onTap: () {
                    menuProvider.changeSubTab(2);
                  },
                ),

                // Text(
                //   '--------------------',
                //   style: TextStyle(color: Colors.grey),
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Tab extends StatelessWidget {
  final menuProvider = getIt<MenuProvider>();

  String name;
  IconData icon;
  int index;
  Function onTap;
  Tab({this.name, this.icon, this.index, this.onTap});
  @override
  Widget build(BuildContext context) {
    return _getStream();
  }

  Widget _getStream() {
    return StreamBuilder<int>(
        initialData: -1,
        stream: menuProvider.currentSubTab,
        builder: (context, snapshot) {
          return _buildTab(snapshot.data);
        });
  }

  Widget _buildTab(int selectedIndex) {
    return Container(
      height: 35,
      child: RaisedButton(
        elevation: 0,
        splashColor: Colors.pink.withOpacity(0.5),
        color: Colors.transparent,
        onPressed: onTap,
        child: Container(
          height: 35,
          child: Row(
            children: [
              // CupertinoIcons.skew
              Icon(
                icon,
                // Icons.panorama_wide_angle_rounded,
                color: index == selectedIndex ? Colors.pink : Colors.white,
                size: 20,
              ),
              // GetX<MenuController>(
              //   builder: (_) => Text(_..toString()),
              // ),
              Text(
                '  ${name} ',
                style: TextStyle(
                    color: index == selectedIndex ? Colors.pink : Colors.white,
                    fontSize: 12),
              )
            ],
          ),
        ),
      ),
    );
  }
}
