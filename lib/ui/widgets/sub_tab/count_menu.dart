import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:pipeCount/getIt.dart';
import 'package:pipeCount/src/controllers/counter_screen_controller.dart';
import 'package:pipeCount/src/helper/uploadFileHelper.dart';
import 'package:pipeCount/src/utils/i18n.dart';
import 'package:pipeCount/src/utils/timer.dart';
import 'package:pipeCount/ui/widgets/line.dart';
import 'package:pipeCount/ui/widgets/loading.dart';
import 'package:splash_tap/splash_tap.dart';
import 'package:toast/toast.dart';

class CountMenu extends StatelessWidget {
  final menuProvider = getIt<MenuProvider>();

  @override
  Widget build(BuildContext context) {
    return _areaWidgetAnimation(context);
  }

  Widget _areaWidgetAnimation(BuildContext context) {
    final _timer = Debouncer(milliseconds: 1000);

    void _onLoading() {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return dialogMessage(context, text(context, 'dialog.counting') ,  text(context, 'dialog.wait'),);
        },
      );
    }

    return Container(
      width: Size.infinite.width,
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        color: Colors.black.withOpacity(0.4),
      ),
      child: SingleChildScrollView(
        child: AnimationLimiter(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            // mainAxisSize: MainAxisSize.min,
            children: AnimationConfiguration.toStaggeredList(
              duration: const Duration(milliseconds: 100),
              childAnimationBuilder: (widget) => SlideAnimation(
                horizontalOffset: 40.0,
                child: FadeInAnimation(
                  child: widget,
                ),
              ),
              children: [
                Tab(
                  name: text(context, "button.start_count"),
                  icon: Icons.grain,
                  isActive: false,
                  onTap: () async {
                    _onLoading();
                    final status = await menuProvider.getResultCouting();
                    Navigator.pop(context);
                    if(status == false) {
                      Toast.show("ERROR: Timeout", context,
                          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
                      // _timer.run(() {
                      // });
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Tab extends StatelessWidget {
  String name;
  IconData icon;
  bool isActive;
  Function onTap;
  Tab({this.name, this.icon, this.isActive, this.onTap});
  @override
  Widget build(BuildContext context) {
    return _buildTab();
  }

  Widget _buildTab() {
    return Container(
      height: 35,
      child: RaisedButton(
        elevation: 0,
        splashColor: Colors.pink.withOpacity(0.5),
        color: Colors.transparent,
        onPressed: onTap,
        child: Container(
          height: 35,
          child: Row(
            children: [
              // CupertinoIcons.skew
              Icon(
                icon,
                // Icons.panorama_wide_angle_rounded,
                color: isActive ? Colors.pink : Colors.white,
                size: 20,
              ),
              // GetX<MenuController>(
              //   builder: (_) => Text(_..toString()),
              // ),
              Text(
                '  ${name} ',
                style: TextStyle(
                    color: isActive ? Colors.pink : Colors.white, fontSize: 12),
              )
            ],
          ),
        ),
      ),
    );
  }
}
