import 'package:flutter/material.dart';
import 'package:pipeCount/getIt.dart';
import 'package:pipeCount/src/controllers/counter_screen_controller.dart';

import 'area_menu.dart';
import 'count_menu.dart';
import 'tags_menu.dart';

class SubBottomTab extends StatefulWidget {
  @override
  _SubBottomTabState createState() => _SubBottomTabState();
}

class _SubBottomTabState extends State<SubBottomTab> {
  final menuProvider = getIt<MenuProvider>();

  @override
  Widget build(BuildContext context) {
    return _getStream();
  }

  Widget _getStream() {
    return StreamBuilder<int>(
        initialData: 1,
        stream: menuProvider.currentMainTab,
        builder: (context, snapshot) {
          return buildTab(snapshot.data);
        });
  }

  Widget buildTab(int currentTab) {
    if (currentTab == 0) return AreaMenu();
    if (currentTab == 1) return CountMenu();
    if (currentTab == 2) return TagsMenu();
    return Container();
  }
}
