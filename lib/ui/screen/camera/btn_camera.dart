import 'package:animator/animator.dart';
import 'package:flutter/material.dart';

class BtnCamera extends StatefulWidget {
  Function onTap;
  BtnCamera({this.onTap});
  @override
  _BtnCameraState createState() => _BtnCameraState();
}

class _BtnCameraState extends State<BtnCamera> {
  @override
  Widget build(BuildContext context) {
    return _captureControlWithAnimation();
  }

  Widget _captureControlWithAnimation() {
    return Animator<double>(
      resetAnimationOnRebuild: true,
      duration: Duration(milliseconds: 200),
      // cycles: 2,
      builder: (_, anim, __) => Center(
        child: Transform.scale(
          scale: 1,
          child: _captureControlRowWidget(anim.value),
        ),
      ),
    );
  }

  Widget _captureControlRowWidget(double scale) {
    return InkWell(
      onTap: () {
        widget.onTap();
      },
      child: Container(
        decoration: BoxDecoration(
          // border: Border.all(color: Colors.blueAccent, width: 2),
          borderRadius: BorderRadius.circular(11),
          color: Colors.black.withOpacity(0.3),
        ),
        width: 60,
        height: 60,
        child: Center(
            child: Container(
          height: 15 + scale * 10,
          width: 15 + scale * 10,
          decoration: BoxDecoration(
            // border: Border.all(color: Colors.white, width: 2),
            borderRadius: BorderRadius.circular(40),
            color: Colors.pink,
          ),
        )),
      ),
    );
  }
}

//
// controller != null && controller.value.isInitialized
// ? onTakePictureButtonPressed
//     : null,
