// Copyright 2019 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// ignore_for_file: public_member_api_docs

import 'dart:async';
import 'dart:io';

import 'package:animator/animator.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pipeCount/getIt.dart';
import 'package:pipeCount/src/controllers/counter_screen_controller.dart';
import 'package:pipeCount/src/controllers/image_controller.dart';
import 'package:pipeCount/src/models/circle.dart';
import 'package:pipeCount/src/utils/i18n.dart';
import 'package:pipeCount/src/utils/sharedPref.dart';
import 'package:pipeCount/ui/screen/camera/menu.dart';
import 'package:pipeCount/ui/screen/count/ImageEditor.dart';
import 'package:pipeCount/ui/screen/count/editScreen.dart';
import 'package:pipeCount/ui/screen/settings/settings.dart';
import 'package:pipeCount/ui/screen/splash/splash.dart';
import 'package:pipeCount/ui/widgets/dialog.dart';
import 'package:pipeCount/ui/widgets/loading.dart';
import 'package:toast/toast.dart';

import 'btn_camera.dart';

class CaptureScreen extends StatefulWidget {
  final menuProvider = getIt<MenuProvider>();
  MyHomePage myHomePage;
  final String imagePath;
  CaptureScreen({this.imagePath});
  @override
  _CaptureScreenState createState() {
    return _CaptureScreenState();
  }
}

void logError(String code, String message) =>
    print('Error: $code\nError Message: $message');

class _CaptureScreenState extends State<CaptureScreen>
    with WidgetsBindingObserver {
  String imagePath;
  int currentTab = 0;

  @override
  void initState() {
    super.initState();
    imagePath = widget.imagePath;
    widget.menuProvider.imagePath = imagePath;
    widget.menuProvider.tagSize = SharePref.prefs.getDouble('tags.default_tag_size') ??
        50;
    widget.menuProvider.isShowNumber = SharePref.prefs.getBool('tags.show_tag_number') ?? true;
    widget.menuProvider.isSaveOriginalImage =  SharePref.prefs.getBool('tags.save_original_photo') ?? false;
    widget.menuProvider.background =  Color(SharePref.prefs.getInt('tags.background') ?? Colors.blue.withOpacity(0.7).value);
    widget.menuProvider.color =  Color(SharePref.prefs.getInt('tags.color') ?? Colors.white.value);
    // WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () {
        widget.menuProvider.reset();
        setState(() {
          imagePath = null;
        });
        Navigator.of(context).pushReplacement(PageRouteBuilder(
            pageBuilder: (context, animation, anotherAnimation) {
              return Splash();
            },
            transitionDuration: Duration(milliseconds: 700),
            transitionsBuilder: (context, animation, anotherAnimation, child) {
              animation = CurvedAnimation(
                  curve: Curves.fastOutSlowIn, parent: animation);
              return Align(
                  child: FadeTransition(
                opacity: animation,
                child: child,
              ));
            }));
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(0.0),
            child: AppBar(
              automaticallyImplyLeading: false, // hides leading widget
              flexibleSpace: Container(),
            )),
        body: Stack(
          children: <Widget>[
            Container(
              height: size.height,
              child: GestureDetector(
                onTap: () {
                  // widget.menuController.reset();
                },
                child: Center(child: _cameraPreviewWidget(widget.myHomePage)
                    // Animator<double>(
                    //   resetAnimationOnRebuild: true,
                    //   duration: Duration(milliseconds: 200),
                    //   // cycles: 2,
                    //   builder: (_, anim, __) => Center(
                    //     child: Transform.scale(
                    //       scale: anim.value,
                    //       child: _cameraPreviewWidget(),
                    //     ),
                    //   ),
                    // ),
                    ),
              ),
              decoration: BoxDecoration(
                color: Colors.black,
              ),
            ),
            StreamBuilder(
                initialData: false,
                stream: widget.menuProvider.setting,
                builder: (context, snapshot) {
                  return snapshot.data ? Container() : _swichControlRowWidget();
                }),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: _controlBottomWidget(),
            ),
            // Positioned(bottom: 150, left: 90, child: _areaWidgetAnimation())
          ],
        ),
      ),
    );
  }

  Widget _cameraPreviewWidget(MyHomePage myHomePage) {
    final size = MediaQuery.of(context).size;
    final deviceRatio = size.width / size.height;
    double xScale = 1;
    // if (controller != null) xScale = controller.value.aspectRatio / deviceRatio;
// Modify the yScale if you are in Landscape
//     double yScale = controller.value.aspectRatio / deviceRatio;
    return imagePath == null
        ? GestureDetector(child: Container())
        : Animator<double>(
            resetAnimationOnRebuild: true,
            duration: Duration(milliseconds: 300),
            // cycles: 2,
            builder: (_, anim, __) {
              myHomePage = MyHomePage(
                pathImage: imagePath,
              );
              return Center(
                // child: Transform.scale(
                //   scale: anim.value,
                //   child: AspectRatio(
                //       aspectRatio: deviceRatio,
                //       child: Transform(
                //         alignment: Alignment.center,
                //         transform: Matrix4.diagonal3Values(xScale, yScale, 1),
                //         child: MyHomePage(
                //           pathImage: imagePath,
                //         ),
                //       )),
                // ),
                child: myHomePage
              );
            },
          );
  }

  Widget _controlBottomWidget() {
    return imagePath == null
        ? Container(
            color: Colors.transparent,
            height: 80,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                // _thumbnailWidget(),
                // _getFromGallery(),
                // BtnCamera(
                //   onTap: controller != null && controller.value.isInitialized
                //       ? onTakePictureButtonPressed
                //       : null,
                // ),
                // _swichControlIcon()
              ],
            ),
          )
        : Animator<double>(
            resetAnimationOnRebuild: true,
            duration: Duration(milliseconds: 300),
            // cycles: 2,
            builder: (_, anim, __) => Center(
              child: Transform.scale(
                scale: anim.value,
                child: Menu(),
              ),
            ),
          );
  }

  /// Display the thumbnail of the captured image or video.
  Widget _thumbnailWidget() {
    Future getImage() async {
      return await ImagePicker.pickImage(source: ImageSource.gallery);
    }

    double height = 55.0;
    double width = 55.0;
    return InkWell(
      onTap: getImage,
      child: Container(
        height: height,
        width: width,
        child: Expanded(
          child: Align(
            alignment: Alignment.centerRight,
            child:
                // imagePath == null
                //     ? Container(
                //         height: height,
                //         width: width,
                //       )
                //     :
                //     Container(
                //         decoration: BoxDecoration(
                //             border: Border.all(color: Colors.pink),
                //             shape: BoxShape.circle,
                //             image: DecorationImage(
                //                 image: FileImage(File(imagePath)),
                //                 fit: BoxFit.cover)
                //             // color: Colors.grey.withOpacity(0.4),
                //             ),
                //       ),
                IconButton(
              icon: const Icon(Icons.flip_camera_ios_outlined),
              color: Colors.white,
              iconSize: 30,
            ),
            // Image.file(
            //   File(imagePath),
            //   fit: BoxFit.fill,
            // ),
          ),
        ),
      ),
    );
  }

  Widget _swichControlRowWidget() {
    void _onLoading() {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return dialogMessage(context, "Saving", text(context, 'dialog.wait'));
        },
      );
    }
    return Container(
      // color: Colors.black,
      padding: EdgeInsets.only(top: 15, right: 10, left: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
              // color: Colors.blue,
              child: imagePath == null
                  ? Container(
                      child: Text(""),
                    )
                  : Container(
                      width: 45,
                      height: 45,
                      decoration: BoxDecoration(
                        // border: Border.all(color: Colors.grey),
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.black.withOpacity(0.3),
                      ),
                      child: IconButton(
                        icon: Icon(
                          Icons.arrow_back_rounded,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          widget.menuProvider.reset();
                          setState(() {
                            imagePath = null;
                          });
                          Navigator.of(context).pushReplacement(
                              PageRouteBuilder(
                                  pageBuilder:
                                      (context, animation, anotherAnimation) {
                                    return Splash();
                                  },
                                  transitionDuration:
                                      Duration(milliseconds: 700),
                                  transitionsBuilder: (context, animation,
                                      anotherAnimation, child) {
                                    animation = CurvedAnimation(
                                        curve: Curves.fastOutSlowIn,
                                        parent: animation);
                                    return Align(
                                        child: FadeTransition(
                                      opacity: animation,
                                      child: child,
                                    ));
                                  }));
                        },
                      ),
                    )),
          Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(8),
              color: Colors.black.withOpacity(0.3),
            ),
            child: StreamBuilder<int>(
                stream: widget.menuProvider.result,
                initialData: widget.menuProvider.totalTag,
                builder: (context, snapshot) {
                  return Container(
                      // width: 50,
                      child: Text(
                    '${text(context, "title.result")}: ${snapshot.data}',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ));
                }),
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                // RaisedButton(
                //   elevation: 0,
                //   splashColor: Colors.pink,
                //   color: Colors.transparent,
                //   onPressed: () {
                //     widget.menuProvider.ScreenShot();
                //   },
                //   child: Text(text(context, "title.save"),
                //       style: TextStyle(color: Colors.white)),
                // ),
                Container(
                  width: 45,
                  height: 45,
                  decoration: BoxDecoration(
                    // border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.black.withOpacity(0.3),
                  ),
                  child: Center(
                    child: IconButton(
                      icon: Icon(
                        Icons.save,
                        color: Colors.white,
                      ),
                      splashColor: Colors.pink,
                      color: Colors.transparent,
                      onPressed: () async {
                        print('save');
                        _onLoading();
                        widget.menuProvider.ScreenShot().then((value) {
                          Navigator.pop(context);
                          Toast.show("Save Success", context,
                              duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
                        });
                      },
                    ),
                  ),
                ),
                Container(
                  width: 45,
                  height: 45,
                  decoration: BoxDecoration(
                    // border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.black.withOpacity(0.3),
                  ),
                  child: Center(
                    child: IconButton(
                      icon: Icon(
                        Icons.settings,
                        color: Colors.white,
                      ),
                      splashColor: Colors.pink,
                      color: Colors.transparent,
                      onPressed: () async {
                        widget.menuProvider.changeSetting(true);
                        DialogUtil.showDialogSettingLeft(context, Settings());
                      },
                    ),
                  ),
                ),
              ])
        ],
      ),
    );
  }
}
