import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:pipeCount/getIt.dart';
import 'package:pipeCount/src/controllers/counter_screen_controller.dart';

class SensitivitySlider extends StatelessWidget {
  final String type;
  SensitivitySlider({this.type});
  final menuProvider = getIt<MenuProvider>();

  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: AnimationLimiter(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: AnimationConfiguration.toStaggeredList(
              duration: const Duration(milliseconds: 100),
              childAnimationBuilder: (widget) => SlideAnimation(
                    horizontalOffset: 40.0,
                    child: FadeInAnimation(
                      child: widget,
                    ),
                  ),
              children: slider(size.width)),
        ),
      ),
    );
  }

  List<Widget> slider(double width) {
    return [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Text(
          '${type}',
          style: TextStyle(color: Colors.pink, fontSize: 13),
        ),
      ),
      Container(
        width: 0.7 * width,
        child: CustomSlider(
          type: type,
          initValue: menuProvider.tagConfidence.toInt(),
          onChange: (double newValue) {
            // if (type == "Size tags")
            menuProvider.changeConficenceOfTags(newValue);
            menuProvider.changeSubTab(int.parse(menuProvider.subTab));
          },
        ),
      ),
      Container(
        width: 10,
        height: 1,
      ),
    ];
  }
}

class SizeSlider extends StatelessWidget {
  final String type;
  SizeSlider({this.type});
  final menuProvider = getIt<MenuProvider>();

  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: AnimationLimiter(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: AnimationConfiguration.toStaggeredList(
              duration: const Duration(milliseconds: 100),
              childAnimationBuilder: (widget) => SlideAnimation(
                horizontalOffset: 40.0,
                child: FadeInAnimation(
                  child: widget,
                ),
              ),
              children: slider(size.width)),
        ),
      ),
    );
  }

  List<Widget> slider(double width) {
    return [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Text(
          '${type}',
          style: TextStyle(color: Colors.pink, fontSize: 13),
        ),
      ),
      Container(
        width: 0.7 * width,
        child: CustomSlider(
          type: type,
          initValue: menuProvider.tagSize.toInt(),
          onChange: (double newValue) {
            // if (type == "Size tags")
            menuProvider.changeSizeOfTags(newValue);
            menuProvider.changeSubTab(int.parse(menuProvider.subTab));
          },
        ),
      ),
      Container(
        width: 10,
        height: 1,
      ),
    ];
  }
}

class CustomSlider extends StatefulWidget {
  Function onChange;
  String type;
  int initValue;
  CustomSlider({this.type, this.initValue, this.onChange});
  @override
  _CustomSliderState createState() => _CustomSliderState();
}

class _CustomSliderState extends State<CustomSlider> {
  int _currentSliderValue = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _currentSliderValue = widget.initValue;
  }

  @override
  Widget build(BuildContext context) {
    return SliderTheme(
      data: SliderTheme.of(context).copyWith(
          thumbShape: RoundSliderThumbShape(enabledThumbRadius: 7.0),
          overlayShape: RoundSliderOverlayShape(overlayRadius: 11.0),
          activeTrackColor: Colors.pink,
          thumbColor: Colors.pink,
          valueIndicatorTextStyle: TextStyle(color: Colors.pink),
          trackHeight: 2),
      child: Slider(
        value: _currentSliderValue.toDouble(),
        min: 0,
        max: 100,
        label: _currentSliderValue.toString(),
        divisions: 100,
        // activeColor: Colors.white,
        // inactiveColor: Color(0xFF8D8E98),
        onChanged: (double newValue) {
          widget.onChange(newValue);
          setState(() {
            _currentSliderValue = newValue.round();
          });
        },
      ),
    );
  }
}
