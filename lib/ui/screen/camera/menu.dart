import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:pipeCount/getIt.dart';
import 'package:pipeCount/src/controllers/counter_screen_controller.dart';
import 'package:pipeCount/ui/screen/camera/animation.dart';
import 'package:pipeCount/ui/screen/camera/slider.dart';
import 'package:pipeCount/ui/widgets/sub_tab/action_menu.dart';
import 'package:pipeCount/ui/widgets/sub_tab/sub_tab.dart';
import 'package:pipeCount/ui/widgets/sub_tab/tags_menu.dart';
import 'package:pipeCount/ui/widgets/line.dart';
import 'package:pipeCount/ui/widgets/bottom_tab.dart';

import '../../widgets/sub_tab/area_menu.dart';
import '../../widgets/sub_tab/count_menu.dart';

class Menu extends StatefulWidget {
  int currentTab = 0;

  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> with SingleTickerProviderStateMixin {
  TabController tabController;
  final menuProvider = getIt<MenuProvider>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ActionMenu(),
        Line(axis: Axis.horizontal),
        SubBottomTab(),
        Line(axis: Axis.horizontal),
        BottomTab(),
      ],
    );
  }

}
