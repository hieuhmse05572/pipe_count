// // Copyright 2019 The Chromium Authors. All rights reserved.
// // Use of this source code is governed by a BSD-style license that can be
// // found in the LICENSE file.
//
// // ignore_for_file: public_member_api_docs
//
// import 'dart:async';
// import 'dart:io';
//
// import 'package:animator/animator.dart';
// import 'package:camera/camera.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:image_picker/image_picker.dart';
// import 'package:path_provider/path_provider.dart';
// import 'package:pipeCount/getIt.dart';
// import 'package:pipeCount/src/controllers/counter_screen_controller.dart';
// import 'package:pipeCount/src/controllers/image_controller.dart';
// import 'package:pipeCount/ui/screen/camera/menu.dart';
// import 'package:pipeCount/ui/screen/count/ImageEditor.dart';
// import 'package:pipeCount/ui/screen/count/editScreen.dart';
// import 'package:pipeCount/ui/screen/settings/settings.dart';
// import 'package:pipeCount/ui/screen/splash/splash.dart';
// import 'package:pipeCount/ui/widgets/dialog.dart';
//
// import 'btn_camera.dart';
//
// List<CameraDescription> cameras = [];
//
// class CaptureScreen extends StatefulWidget {
//   final menuProvider = getIt<MenuProvider>();
//
//   final String imagePath;
//   CaptureScreen({this.imagePath});
//   @override
//   _CaptureScreenState createState() {
//     return _CaptureScreenState();
//   }
// }
//
// void logError(String code, String message) =>
//     print('Error: $code\nError Message: $message');
//
// class _CaptureScreenState extends State<CaptureScreen>
//     with WidgetsBindingObserver {
//   CameraController controller;
//   String imagePath;
//   int currentCamera = 0;
//   int currentTab = 0;
//
//   @override
//   void initState() {
//     super.initState();
//     initCamera();
//     imagePath = widget.imagePath;
//     // WidgetsBinding.instance.addObserver(this);
//   }
//
//   @override
//   void dispose() {
//     WidgetsBinding.instance.removeObserver(this);
//     super.dispose();
//   }
//
//   // @override
//   // void didChangeAppLifecycleState(AppLifecycleState state) {
//   //   // App state changed before we got the chance to initialize.
//   //   if (controller == null || !controller.value.isInitialized) {
//   //     return;
//   //   }
//   //   if (state == AppLifecycleState.inactive) {
//   //     controller?.dispose();
//   //   } else if (state == AppLifecycleState.resumed) {
//   //     if (controller != null) {
//   //       onNewCameraSelected(controller.description);
//   //     }
//   //   }
//   // }
//
//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//
//   @override
//   Widget build(BuildContext context) {
//     Size size = MediaQuery.of(context).size;
//     return Scaffold(
//       key: _scaffoldKey,
//       appBar: PreferredSize(
//           preferredSize: Size.fromHeight(0.0),
//           child: AppBar(
//             automaticallyImplyLeading: false, // hides leading widget
//             flexibleSpace: Container(),
//           )),
//       body: Stack(
//         children: <Widget>[
//           Container(
//             height: size.height,
//             child: GestureDetector(
//               onTap: () {
//                 // widget.menuController.reset();
//               },
//               child: Center(child: _cameraPreviewWidget()
//                   // Animator<double>(
//                   //   resetAnimationOnRebuild: true,
//                   //   duration: Duration(milliseconds: 200),
//                   //   // cycles: 2,
//                   //   builder: (_, anim, __) => Center(
//                   //     child: Transform.scale(
//                   //       scale: anim.value,
//                   //       child: _cameraPreviewWidget(),
//                   //     ),
//                   //   ),
//                   // ),
//                   ),
//             ),
//             decoration: BoxDecoration(
//               color: Colors.black,
//             ),
//           ),
//           StreamBuilder(
//               initialData: false,
//               stream: widget.menuProvider.setting,
//               builder: (context, snapshot) {
//                 return snapshot.data ? Container() : _swichControlRowWidget();
//               }),
//
//           // _captureControlRowWidget(),
//           // imagePath == null
//           //     ? _swichControlRowWidget()
//           // Container(
//           //         height: 60,
//           //         child: Animator(
//           //           tween: Tween<Offset>(
//           //               begin: Offset(0, -30), end: Offset(0, 30)),
//           //           resetAnimationOnRebuild: true,
//           //           duration: Duration(milliseconds: 100),
//           //           // cycles: 2,
//           //           builder: (_, anim, __) => Center(
//           //             child: Transform.translate(
//           //               offset: anim.value,
//           //               child: _swichControlRowWidget(),
//           //             ),
//           //           ),
//           //         ),
//           //       )
//           // : Container(),
//
//           Positioned(
//             bottom: 0,
//             left: 0,
//             right: 0,
//             child: _controlBottomWidget(),
//           ),
//           // Positioned(bottom: 150, left: 90, child: _areaWidgetAnimation())
//         ],
//       ),
//     );
//   }
//
//   Widget _cameraPreviewWidget() {
//     final size = MediaQuery.of(context).size;
//     final deviceRatio = size.width / size.height;
//     double xScale = 1;
//     if (controller != null) xScale = controller.value.aspectRatio / deviceRatio;
// // Modify the yScale if you are in Landscape
//     double yScale = controller.value.aspectRatio / deviceRatio;
//     return imagePath == null
//         ? GestureDetector(
//             onDoubleTap: controller != null && controller.value.isInitialized
//                 ? onTakePictureButtonPressed
//                 : null,
//             child: Container(
//               child: AspectRatio(
//                   aspectRatio: deviceRatio,
//                   child: Transform(
//                     alignment: Alignment.center,
//                     transform: Matrix4.diagonal3Values(xScale, yScale, 1),
//                     child: controller == null
//                         ? Container()
//                         : CameraPreview(controller),
//                   )),
//             ),
//           )
//         : Animator<double>(
//             resetAnimationOnRebuild: true,
//             duration: Duration(milliseconds: 300),
//             // cycles: 2,
//             builder: (_, anim, __) {
//               return Center(
//
//                 // child: Transform.scale(
//                 //   scale: anim.value,
//                 //   child: AspectRatio(
//                 //       aspectRatio: deviceRatio,
//                 //       child: Transform(
//                 //         alignment: Alignment.center,
//                 //         transform: Matrix4.diagonal3Values(xScale, yScale, 1),
//                 //         child: MyHomePage(
//                 //           pathImage: imagePath,
//                 //         ),
//                 //       )),
//                 // ),
//                 child: MyHomePage(
//                   pathImage: imagePath,
//                 ),
//               );
//             },
//           );
//   }
//
//   Widget _controlBottomWidget() {
//     return imagePath == null
//         ? Container(
//             color: Colors.transparent,
//             height: 80,
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceAround,
//               children: <Widget>[
//                 // _thumbnailWidget(),
//                 _getFromGallery(),
//                 BtnCamera(
//                   onTap: controller != null && controller.value.isInitialized
//                       ? onTakePictureButtonPressed
//                       : null,
//                 ),
//                 _swichControlIcon()
//               ],
//             ),
//           )
//         : Animator<double>(
//             resetAnimationOnRebuild: true,
//             duration: Duration(milliseconds: 300),
//             // cycles: 2,
//             builder: (_, anim, __) => Center(
//               child: Transform.scale(
//                 scale: anim.value,
//                 child: Menu(),
//               ),
//             ),
//           );
//   }
//
//   /// Display the thumbnail of the captured image or video.
//   Widget _thumbnailWidget() {
//     Future getImage() async {
//       return await ImagePicker.pickImage(source: ImageSource.gallery);
//     }
//
//     double height = 55.0;
//     double width = 55.0;
//     return InkWell(
//       onTap: getImage,
//       child: Container(
//         height: height,
//         width: width,
//         child: Expanded(
//           child: Align(
//             alignment: Alignment.centerRight,
//             child:
//                 // imagePath == null
//                 //     ? Container(
//                 //         height: height,
//                 //         width: width,
//                 //       )
//                 //     :
//                 //     Container(
//                 //         decoration: BoxDecoration(
//                 //             border: Border.all(color: Colors.pink),
//                 //             shape: BoxShape.circle,
//                 //             image: DecorationImage(
//                 //                 image: FileImage(File(imagePath)),
//                 //                 fit: BoxFit.cover)
//                 //             // color: Colors.grey.withOpacity(0.4),
//                 //             ),
//                 //       ),
//                 IconButton(
//               icon: const Icon(Icons.flip_camera_ios_outlined),
//               color: Colors.white,
//               iconSize: 30,
//               onPressed: () {
//                 if (currentCamera == 1)
//                   currentCamera = 0;
//                 else
//                   currentCamera = 1;
//                 CameraDescription cameraDescription = cameras[currentCamera];
//                 onNewCameraSelected(cameraDescription);
//               },
//             ),
//             // Image.file(
//             //   File(imagePath),
//             //   fit: BoxFit.fill,
//             // ),
//           ),
//         ),
//       ),
//     );
//   }
//
//   Widget _swichControlRowWidget() {
//     return Container(
//       // color: Colors.black,
//       padding: EdgeInsets.only(top: 15, right: 10, left: 15),
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: <Widget>[
//           Container(
//               // color: Colors.blue,
//               child: imagePath == null
//                   ? Container(
//                       child: Text(""),
//                     )
//                   : Container(
//                       width: 45,
//                       height: 45,
//                       decoration: BoxDecoration(
//                         // border: Border.all(color: Colors.grey),
//                         borderRadius: BorderRadius.circular(8),
//                         color: Colors.black.withOpacity(0.3),
//                       ),
//                       child: IconButton(
//                         icon: Icon(
//                           Icons.arrow_back_rounded,
//                           color: Colors.white,
//                         ),
//                         onPressed: () {
//                           // Navigator.of(context).pushReplacement(PageRouteBuilder(
//                           //     pageBuilder:
//                           //         (context, animation, anotherAnimation) {
//                           //       return Splash();
//                           //     },
//                           //     transitionDuration: Duration(milliseconds: 700),
//                           //     transitionsBuilder:
//                           //         (context, animation, anotherAnimation, child) {
//                           //       animation = CurvedAnimation(
//                           //           curve: Curves.fastOutSlowIn,
//                           //           parent: animation);
//                           //       return Align(
//                           //           child: FadeTransition(
//                           //         opacity: animation,
//                           //         child: child,
//                           //       ));
//                           //     }));
//                           widget.menuProvider.reset();
//                           setState(() {
//                             imagePath = null;
//                           });
//                         },
//                       ),
//                     )),
//           Container(
//             child: Row(
//               mainAxisSize: MainAxisSize.min,
//               children: [
//                 // StreamBuilder<String>(
//                 //     initialData: "",
//                 //     stream: widget.menuProvider.current,
//                 //     builder: (context, snapshot) {
//                 //       return Container(
//                 //         width: 50,
//                 //         child: RaisedButton(
//                 //           child: Icon(
//                 //             widget.menuProvider.isShowNumber
//                 //                 ? Icons.visibility_sharp
//                 //                 : Icons.visibility_off_sharp,
//                 //             color: Colors.white,
//                 //           ),
//                 //           elevation: 0,
//                 //           splashColor: Colors.pink,
//                 //           color: Colors.transparent,
//                 //           onPressed: () {
//                 //             widget.menuProvider.isShowNumber =
//                 //                 !widget.menuProvider.isShowNumber;
//                 //             widget.menuProvider.refresh();
//                 //           },
//                 //         ),
//                 //       );
//                 //     }),
//
//                 Container(
//                   width: 45,
//                   height: 45,
//                   decoration: BoxDecoration(
//                     // border: Border.all(color: Colors.grey),
//                     borderRadius: BorderRadius.circular(8),
//                     color: Colors.black.withOpacity(0.3),
//                   ),
//                   child: Center(
//                     child: IconButton(
//                       icon: Icon(
//                         Icons.settings,
//                         color: Colors.white,
//                       ),
//                       splashColor: Colors.pink,
//                       color: Colors.transparent,
//                       onPressed: () async {
//                         widget.menuProvider.changeSetting(true);
//                         DialogUtil.showDialogSettingLeft(context, Settings());
//                       },
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }
//
//   Widget _swichControlIcon() {
//     return Container(
//       height: 45,
//       width: 45,
//       decoration: BoxDecoration(
//         // border: Border.all(color: Colors.grey),
//         borderRadius: BorderRadius.circular(8),
//         color: Colors.black.withOpacity(0.3),
//       ),
//       child: IconButton(
//         icon: const Icon(Icons.flip_camera_ios_outlined),
//         color: Colors.white,
//         iconSize: 25,
//         onPressed: () {
//           if (currentCamera == 1)
//             currentCamera = 0;
//           else
//             currentCamera = 1;
//           CameraDescription cameraDescription = cameras[currentCamera];
//           onNewCameraSelected(cameraDescription);
//         },
//       ),
//     );
//   }
//
//   Widget _getFromGallery() {
//     Future getImage() async {
//       return await ImagePicker.pickImage(source: ImageSource.gallery);
//     }
//
//     return Container(
//       height: 45,
//       width: 45,
//       decoration: BoxDecoration(
//         // border: Border.all(color: Colors.grey),
//         borderRadius: BorderRadius.circular(8),
//         color: Colors.black.withOpacity(0.3),
//       ),
//       child: IconButton(
//         icon: const Icon(Icons.photo_library),
//         color: Colors.white,
//         iconSize: 25,
//         onPressed: () async {
//           File image = await getImage();
//           setState(() {
//             imagePath = image.path;
//           });
//         },
//       ),
//     );
//   }
//
//   /// Display the control bar with buttons to take pictures and record videos.
//
//   String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();
//
//   void showInSnackBar(String message) {
//     // ignore: deprecated_member_use
//     _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message)));
//   }
//
//   void onNewCameraSelected(CameraDescription cameraDescription) async {
//     if (controller != null) {
//       await controller.dispose();
//     }
//     controller = CameraController(
//       cameraDescription,
//       ResolutionPreset.medium,
//       enableAudio: false,
//     );
//
//     // If the controller is updated then update the UI.
//     controller.addListener(() {
//       if (mounted) setState(() {});
//       if (controller.value.hasError) {
//         showInSnackBar('Camera error ${controller.value.errorDescription}');
//       }
//     });
//
//     try {
//       await controller.initialize();
//     } on CameraException catch (e) {
//       _showCameraException(e);
//     }
//
//     if (mounted) {
//       setState(() {});
//     }
//   }
//
//   void onTakePictureButtonPressed() {
//     takePicture().then((String filePath) {
//       if (mounted) {
//         setState(() {
//           imagePath = filePath;
//           widget.menuProvider.imagePath = filePath;
//           if (currentTab == 0)
//             currentTab = 1;
//           else
//             currentTab = 0;
//         });
//         // if (filePath != null) showInSnackBar('Picture saved to $filePath');
//       }
//     });
//   }
//
//   Future<String> takePicture() async {
//     if (!controller.value.isInitialized) {
//       showInSnackBar('Error: select a camera first.');
//       return null;
//     }
//
//     final Directory extDir = await getApplicationDocumentsDirectory();
//     final String dirPath = '${extDir.path}/Pictures/flutter_test';
//     await Directory(dirPath).create(recursive: true);
//     final String filePath = '$dirPath/${timestamp()}.jpg';
//     print(filePath);
//
//     if (controller.value.isTakingPicture) {
//       // A capture is already pending, do nothing.
//       return null;
//     }
//
//     try {
//       await controller.takePicture(filePath);
//     } on CameraException catch (e) {
//       _showCameraException(e);
//       return null;
//     }
//     return filePath;
//   }
//
//   void _showCameraException(CameraException e) {
//     logError(e.code, e.description);
//     showInSnackBar('Error: ${e.code}\n${e.description}');
//   }
//
//   Future<void> initCamera() async {
//     try {
//       WidgetsFlutterBinding.ensureInitialized();
//       cameras = await availableCameras();
//       onNewCameraSelected(cameras[currentCamera]);
//     } on CameraException catch (e) {
//       logError(e.code, e.description);
//     }
//   }
// }
