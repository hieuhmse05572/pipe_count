import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

class AnimationList extends StatelessWidget {
  List<Widget> children;
  AnimationList({this.children});
  @override
  Widget build(BuildContext context) {
    return getAnimation();
  }

  Widget getAnimation() {
    return SingleChildScrollView(
      child: AnimationLimiter(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          // mainAxisSize: MainAxisSize.min,
          children: AnimationConfiguration.toStaggeredList(
            duration: const Duration(milliseconds: 100),
            childAnimationBuilder: (widget) => SlideAnimation(
              horizontalOffset: 40.0,
              child: FadeInAnimation(
                child: widget,
              ),
            ),
            children: children,
          ),
        ),
      ),
    );
  }
}
