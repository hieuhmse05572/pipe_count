import 'dart:io';

import 'package:after_layout/after_layout.dart';
import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pipeCount/src/controllers/counter_screen_controller.dart';
import 'package:pipeCount/src/controllers/image_controller.dart';
import 'package:pipeCount/src/helper/loginHelper.dart';
import 'package:pipeCount/src/repo/LoginApi.dart';
import 'package:pipeCount/src/utils/sharedPref.dart';
import 'package:pipeCount/ui/screen/camera/camera2.dart';
import 'package:pipeCount/ui/screen/count/ImageEditor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:permission_handler/permission_handler.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> with AfterLayoutMixin {
  bool isError = false;
  bool isLoading = true;
  bool loaded = false;

  // final provider = getIt<LoginProvider>();
  // ImageController imageController = Get.put(ImageController());
  // MenuController menuController = Get.put(MenuController());
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SharePref.init();

    Future.delayed(Duration.zero, () async {
      Locale locale;
      if (await Permission.camera.request().isGranted) {}
      if (await Permission.storage.request().isGranted) {}
      // String lang = (prefs.getString('lang') ?? "en");
      String lang = SharePref.prefs.getString('lang');
      if (lang == null)
        locale = await Devicelocale.currentAsLocale;
      else
        locale = Locale(lang);
      await FlutterI18n.refresh(context, locale);
      setState(() {
        loaded = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // SizeConfig().init(context);
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        color: Colors.blueGrey,
        width: size.width,
        height: size.height,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: size.height * 0.25,
              width: 1,
            ),
            Container(
                alignment: Alignment.topCenter,
                width: size.width,
                height: size.height * 0.5,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(11),
                          image: DecorationImage(
                            image: AssetImage("assets/icon/icon1.png"),
                          )),
                      width: 160,
                      height: 110,
                    ),
                    SizedBox(
                      height: 20,
                      width: 1,
                    ),
                    Text(
                      'Pipe Count',
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 20,
                      ),
                    )
                  ],
                )),
            _popupSelectSource(),
          ],
        ),
      ),
    );
  }

  Widget getLoading() {
    return isLoading
        ? Container(
            height: 50,
            child: CupertinoActivityIndicator(
              radius: 14,
            ),
          )
        : Container(
            height: 50,
          );
  }

  Widget getButton() {
    if (!isError) return Container();
    return RaisedButton(
      onPressed: login,
      child: Text(
        FlutterI18n.translate(context, "splash.refresh"),
      ),
    );
  }

  Widget getText() {
    if (!loaded) return Container();
    return isError
        ? Text(
            FlutterI18n.translate(context, "splash.msg"),
            style: TextStyle(color: Colors.grey),
          )
        : Text(
            FlutterI18n.translate(context, "splash.loading"),
            style: TextStyle(color: Colors.grey),
          );
  }

  Widget _popupSelectSource() {
    // return currentTab == 0 ? _areaWidget() : Container();
    Future getImageFromGallery() async {
      File image = await ImagePicker.pickImage(
          source: ImageSource.gallery, imageQuality: 80);
      if (image != null && image.path != null) {
        File rotatedImage =
            await FlutterExifRotation.rotateImage(path: image.path);
        return rotatedImage;
      }
    }

    Future getImageFromCamera() async {
      File image = await ImagePicker.pickImage(
          source: ImageSource.camera, imageQuality: 80);
      if (image != null && image.path != null) {
        File rotatedImage =
            await FlutterExifRotation.rotateImage(path: image.path);
        return rotatedImage;
      }
    }

    return Container(
      height: 120,
      child: AnimationLimiter(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: AnimationConfiguration.toStaggeredList(
            duration: const Duration(milliseconds: 800),
            childAnimationBuilder: (widget) => SlideAnimation(
              horizontalOffset: 40.0,
              child: FadeInAnimation(
                child: widget,
              ),
            ),
            children: [
              RaisedButton(
                elevation: 0,
                // highlightColor: Colors.transparent,
                // hoverColor: Colors.transparent,
                // focusColor: Colors.transparent,
                color: Colors.black.withOpacity(0.2),
                onPressed: () async {
                  File image = await getImageFromCamera();
                  if (image != null)
                    Navigator.of(context).pushReplacement(PageRouteBuilder(
                        pageBuilder: (context, animation, anotherAnimation) {
                          return CaptureScreen(
                            imagePath: image.path,
                          );
                        },
                        transitionDuration: Duration(milliseconds: 0),
                        transitionsBuilder:
                            (context, animation, anotherAnimation, child) {
                          animation = CurvedAnimation(
                              curve: Curves.fastOutSlowIn, parent: animation);
                          return Align(
                              child: FadeTransition(
                            opacity: animation,
                            child: child,
                          ));
                        }));
                },
                child: Container(
                  height: 50,
                  // width: 100,

                  child: Center(
                    child: Text(
                      'Camera',
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 2, bottom: 2),
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(color: Colors.grey, width: 1))),
                height: 1,
                width: Size.infinite.width,
              ),
              RaisedButton(
                elevation: 0,
                color: Colors.black.withOpacity(0.2),
                onPressed: () async {
                  File image = await getImageFromGallery();
                  if (image != null)
                    Navigator.of(context).pushReplacement(PageRouteBuilder(
                        pageBuilder: (context, animation, anotherAnimation) {
                          return CaptureScreen(
                            imagePath: image.path,
                          );
                        },
                        transitionDuration: Duration(milliseconds: 0),
                        transitionsBuilder:
                            (context, animation, anotherAnimation, child) {
                          animation = CurvedAnimation(
                              curve: Curves.fastOutSlowIn, parent: animation);
                          return Align(
                              child: FadeTransition(
                            opacity: animation,
                            child: child,
                          ));
                        }));
                },
                child: Container(
                  height: 50,
                  // width: 100,
                  child: Center(
                    child: Text(
                      'Gallery',
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    ),
                  ),
                ),
              ),
              // Text(
              //   '--------------------',
              //   style: TextStyle(color: Colors.grey),
              // ),
            ],
          ),
        ),
      ),
    );
  }

  void login() {
    setState(() {
      isLoading = true;
    });
    LoginHelper().login("00001", "12345678").then((value) {
      // if (value == true) {
      //   setState(() {
      //     isError = false;
      //   });
      //   Route route = MaterialPageRoute(builder: (context) => CaptureScreen());
      //   Navigator.pushReplacement(context, route);
      // } else {
      Toast.show("Login Success", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      //   setState(() {
      //     isLoading = false;
      //     isError = true;
      //   });
      //   // Route route = MaterialPageRoute(builder: (context) => CaptureScreen());
      //   // Navigator.pushReplacement(context, route);
      // }
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    //   provider.login("00005", "12345678");
    login();
    //   provider.loginStrem.listen((snapshot) {
    //     snapshot.fold((l) {
    //       if (l is NoInternetGlitch) {
    //         Toast.show(Msg.Timeout, context,
    //             duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    //       }
    //     }, (r) {
    //       Toast.show(Msg.LoginSucess, context,
    //           duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    //       Navigator.of(context).pushReplacement(PageRouteBuilder(
    //           pageBuilder: (context, animation, anotherAnimation) {
    //             return OrderScreen();
    //           },
    //           transitionDuration: Duration(milliseconds: 700),
    //           transitionsBuilder: (context, animation, anotherAnimation, child) {
    //             animation = CurvedAnimation(
    //                 curve: Curves.fastOutSlowIn, parent: animation);
    //             return Align(
    //                 child: FadeTransition(
    //               opacity: animation,
    //               child: child,
    //             ));
    //           }));
    //       // Route route = FadeRoute(page: OrderScreen());
    //       // Navigator.pushReplacement(context, route);
    //     });
    //   });
    // }
  }
}
