import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:get/get.dart';
import 'package:pipeCount/src/controllers/image_controller.dart';
import 'dart:async';
import 'dart:typed_data';

import 'package:pipeCount/src/utils/a.dart';
import 'package:pipeCount/ui/screen/count/painter.dart';

import 'ImageEditor.dart';

class EditScreen extends StatefulWidget {
  EditScreen({this.pathImage});
  final String pathImage;

  @override
  _EditScreenState createState() => _EditScreenState();
}

class _EditScreenState extends State<EditScreen> with AfterLayoutMixin {
  final ImageController imageController = Get.find();

  ui.Image image;

  double scaleZoom = 0.75;

  double scale = 1;

  double lastScaleZoom = 1;

  Offset rootOffset = Offset(0, 0);

  Offset lastPoint = Offset(0, 0);

  Future<Null> init() async {
    final data = await Util.readFileByte(widget.pathImage);
    image = await loadImage(Uint8List.view(data.buffer));
    imageController.setIsImageloaded(true);
  }

  Future<ui.Image> loadImage(List<int> img) async {
    final Completer<ui.Image> completer = Completer();
    ui.decodeImageFromList(img, (ui.Image img) {
      return completer.complete(img);
    });
    return completer.future;
  }

  Widget _getPainter() {
    print('dfdfd');
    return MyHomePage(pathImage: widget.pathImage);
    return GetBuilder<ImageController>(
        // init: Get.find<ImageController>(),
        // initState: (state) {},
        builder: (_) {
      ImagePainter editor = ImagePainter(
          image: image, root: rootOffset, scale: scaleZoom, points: _.points);
      return CustomPaint(
        painter: editor,
      );
    });
  }

  Widget _getGesture() {
    return GestureDetector(
      onScaleEnd: (detail) {},
      onScaleStart: (scaleStartDetails) {},
      onScaleUpdate: (scaleUpdateDetails) {
        imageController.onScaleUpdate(scaleUpdateDetails);
      },
    );
  }

  Widget _buildImage() {
    return (imageController.isImageloaded)
        ? Stack(
            children: [
              _getPainter(),
              // _getGesture()
            ],
          )
        : Center(
            child: Text(
            'loading',
            style: TextStyle(color: Colors.white),
          ));
  }

  @override
  Widget build(BuildContext context) {
    print('build');
    return _buildImage();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    init();
  }
}
