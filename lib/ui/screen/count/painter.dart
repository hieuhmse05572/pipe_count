import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:pipeCount/src/utils/hull.dart';

class ImagePainter extends CustomPainter {
  ImagePainter({this.image, this.root, this.scale, this.points});

  ui.Image image;
  Offset root;
  double scale;

  List<Offset> points;

  final Paint painter = new Paint()
    ..color = Colors.black.withOpacity(0.4)
    ..style = PaintingStyle.fill;
  final Paint maskPaint = Paint()
    ..blendMode = BlendMode.clear
    ..strokeWidth = 30
    ..strokeCap = StrokeCap.round;

  void paintImage(
      ui.Image image, Rect outputRect, Canvas canvas, Paint paint, BoxFit fit) {
    final Size imageSize =
        Size(image.width.toDouble(), image.height.toDouble());
    final FittedSizes sizes = applyBoxFit(fit, imageSize, outputRect.size);
    final Rect inputSubrect =
        Alignment.center.inscribe(sizes.source, Offset.zero & imageSize);
    final Rect outputSubrect =
        Alignment.center.inscribe(sizes.destination, outputRect);
    canvas.drawImageRect(image, inputSubrect, outputSubrect, paint);
  }

  void drawHull(List<Offset> hull, Canvas canvas, Paint mask) {
    if (hull.isEmpty) return;
    Path path = new Path();
    path.moveTo(hull[0].dx, hull[0].dy);
    for (int i = 0; i < hull.length - 1; i++) {
      if (hull[i] != null && hull[i + 1] != null) {
        // canvas.drawLine(points[i], points[i + 1], maskPaint);
        path.lineTo(hull[i].dx, hull[i].dy);
      }
    }
    path.close();
    canvas.drawPath(path, mask);
  }

  @override
  void paint(Canvas canvas, Size size) {
    print(points.length);
    paintImage(
        image,
        Rect.fromLTWH(root.dx, root.dy, image.width.toDouble() * scale,
            image.height.toDouble() * scale),
        canvas,
        Paint(),
        BoxFit.contain);

    // if (isInclude) {
    canvas.saveLayer(Offset.zero & size, Paint());
    canvas.drawRect(
        Rect.fromLTWH(root.dx, root.dy, image.width.toDouble() * scale,
            image.height.toDouble() * scale),
        painter);
    //
    //   if (canHull) {
    //     hull = Hull.convexHull(points, points.length);
    //     canHull = false;
    //     // points.add(null);
    //   }
    //   drawHull(canvas, maskPaint);
    for (int i = 0; i < points.length - 1; i++) {
      if (points[i] != null && points[i + 1] != null) {
        canvas.drawLine(points[i], points[i + 1], Paint());
      }
    }
    //   if (hull.length > 0) points.add(null);
    canvas.restore();
    // }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
