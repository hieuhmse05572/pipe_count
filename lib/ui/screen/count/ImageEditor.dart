import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'dart:ui' as ui;
import 'package:image/image.dart' as img;

import 'package:flutter/services.dart' show rootBundle;
import 'package:get/get.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:intl/intl.dart' as intl;
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pipeCount/getIt.dart';
import 'package:pipeCount/src/controllers/counter_screen_controller.dart';
import 'package:pipeCount/src/models/circle.dart';
import 'dart:async';
import 'dart:typed_data';

import 'package:pipeCount/src/utils/a.dart';
import 'package:pipeCount/src/utils/hull.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.pathImage}) : super(key: key);

  final String pathImage;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final menuProvider = getIt<MenuProvider>();
  TransformationController _transformationController =
      TransformationController();

  ui.Image image;
  bool isImageloaded = false;
  GlobalKey _myCanvasKey = new GlobalKey();
  ImageSaver imageSaver;

  double scaleZoom = 1;
  double scale = 1;
  double lastScaleZoom = 1;
  int selectedTag = -1;
  List<Circle> tags = List();

  void initState() {
    super.initState();
    init();
  }

  void _onTapUp(TapUpDetails details) {
    // if (menuProvider.currentStr == '00') return;
    // if (selectedTag > 0) {
    //   tags[selectedTag].isSelected = false;
    //   _myCanvasKey.currentContext.findRenderObject().markNeedsPaint();
    // }
    if (selectedTag >= 0) {
      tags[selectedTag].isSelected = false;
      selectedTag = -1;
      _myCanvasKey.currentContext.findRenderObject().markNeedsPaint();
      return;
    }
    if (menuProvider.actionStr == "add") {
      final Offset pos =
          _transformationController.toScene(details.localPosition);
      Circle circle = Circle(1, pos, 1, menuProvider.tagSize, false);
      int index = tags.indexWhere((element) {
        if ((element.pos.dx - circle.pos.dx).abs() <= menuProvider.tagSize &&
            (element.pos.dy - circle.pos.dy).abs() <= menuProvider.tagSize)
          return true;
        return false;
      });
      if (index == -1) {
        tags.add(circle);
        menuProvider.addTag(circle);
        menuProvider.refresh();
      }
      _myCanvasKey.currentContext.findRenderObject().markNeedsPaint();
    }
  }

  void _onTapDown(TapDownDetails details) {
    if (menuProvider.actionStr == "remove") {
      final Offset pos =
          _transformationController.toScene(details.localPosition);
      tags.removeWhere((element) {
        if ((element.pos.dx - pos.dx).abs() <= menuProvider.tagSize &&
            (element.pos.dy - pos.dy).abs() <= menuProvider.tagSize)
          return true;
        return false;
      });
      menuProvider.removeTag(tags);
      menuProvider.refresh();
      _myCanvasKey.currentContext.findRenderObject().markNeedsPaint();
      return;
    }
    if (menuProvider.currentStr == '00') return;
    final Offset pos = _transformationController.toScene(details.localPosition);
    selectedTag = tags.indexWhere((element) {
      if ((element.pos.dx - pos.dx).abs() <= menuProvider.tagSize &&
          (element.pos.dy - pos.dy).abs() <= menuProvider.tagSize) return true;
      return false;
    });
    if (selectedTag >= 0) tags[selectedTag].isSelected = true;
    _myCanvasKey.currentContext.findRenderObject().markNeedsPaint();
  }

  Future<Null> init() async {
    // final ByteData data = await rootBundle.load('assets/images/bg.jpg');
    final data = await Util.readFileByte(widget.pathImage);
    image = await loadImage(Uint8List.view(data.buffer));
    menuProvider.size = Size(image.width.toDouble(), image.height.toDouble());
    // print("size image");
    // print(image.width.toDouble());
    // print(image.height.toDouble());
  }

  Future<ui.Image> loadImage(List<int> img) async {
    final Completer<ui.Image> completer = Completer();
    ui.decodeImageFromList(img, (ui.Image img) {
      setState(() {
        isImageloaded = true;
      });
      return completer.complete(img);
    });
    return completer.future;
  }

  Widget _buildImage() {
    List<Offset> points = menuProvider.points;
    List<Offset> hull = menuProvider.hull;
    tags = menuProvider.tags;

    if (menuProvider.currentStr == '00') {
      _transformationController.value = Matrix4.identity();
    }
    if (this.isImageloaded) {
      menuProvider.updateResult();
      menuProvider.imageSaver = ImageSaver(
          image: image,
          points: points,
          hull: hull,
          tagSize: menuProvider.tagSize,
          tagConficence: menuProvider.tagConfidence,
          tags: tags,
          background: menuProvider.background,
          color: menuProvider.color,
          isShowNumber: menuProvider.isShowNumber,
          sizeOfScreen: Size(image.width.toDouble(), image.height.toDouble()));
      return FittedBox(
        child: SizedBox(
          width: image.width.toDouble(),
          height: image.height.toDouble(),
          child: GestureDetector(
            onTapDown: _onTapDown,
            // onDoubleTap: () async {
            // },
            onTapUp: _onTapUp,
            // behavior: HitTestBehavior.translucent,az
            child: FittedBox(
                child: SizedBox(
                    width: image.width.toDouble(),
                    height: image.height.toDouble(),
                    child: InteractiveViewer(
                      // boundaryMargin: EdgeInsets.all(20),
                      transformationController: _transformationController,
                      onInteractionEnd: (detail) {
                        if (menuProvider.currentStr == '00') {
                          hull.addAll(Hull.convexHull(points, points.length));
                          menuProvider.addTag(null);
                          menuProvider.refresh();
                        } else {
                          if (selectedTag >= 0) {
                            tags[selectedTag].isSelected = false;
                            selectedTag = -1;
                          }
                        }
                        _myCanvasKey.currentContext
                            .findRenderObject()
                            .markNeedsPaint();
                      },
                      onInteractionStart: (scaleStartDetails) {
                        if (menuProvider.currentStr == '00') {
                          hull.clear();
                          points.clear();
                          // points.add(scaleStartDetails.focalPoint);
                          _myCanvasKey.currentContext
                              .findRenderObject()
                              .markNeedsPaint();
                        }
                      },
                      onInteractionUpdate: (scaleUpdateDetails) {
                        if (menuProvider.currentStr == '00') {
                          points.add(scaleUpdateDetails.focalPoint);
                        } else {
                          if (selectedTag >= 0) {
                            Circle selectedCircle = tags[selectedTag];
                            double dx = (scaleUpdateDetails.focalPoint.dx);
                            double dy = (scaleUpdateDetails.focalPoint.dy);
                            selectedCircle.pos = Offset(dx, dy);
                          }
                        }
                        _myCanvasKey.currentContext
                            .findRenderObject()
                            .markNeedsPaint();
                      },
                      maxScale: 5,
                      minScale: 1,
                      // panEnabled: menuProvider.currentStr == '00' ? false : true,
                      panEnabled: false,
                      scaleEnabled:
                          menuProvider.currentStr == '00' ? false : true,
                      child: CustomPaint(
                        key: _myCanvasKey,
                        painter: ImageEditor(
                            image: image,
                            points: points,
                            hull: hull,
                            tagSize: menuProvider.tagSize,
                            tagConficence: menuProvider.tagConfidence,
                            // tags: points.length > 0 ? innerTags : tags,
                            tags: tags,
                            background: menuProvider.background,
                            color: menuProvider.color,
                            isShowNumber: menuProvider.isShowNumber,
                            sizeOfScreen: Size(image.width.toDouble(),
                                image.height.toDouble())),
                      ),
                    ))),
          ),
        ),
      );
    } else {
      return Center(child: Text('loading'));
    }
  }

  Widget _getStream() {
    return StreamBuilder<String>(
        initialData: "-1",
        stream: menuProvider.current,
        builder: (context, snapshot) {
          return _buildImage();
        });
  }

  @override
  Widget build(BuildContext context) {
    return _getStream();
  }
}

class ImageSaver extends CustomPainter {
  ImageSaver(
      {this.image,
      this.points,
      this.hull,
      this.tags,
      this.tagSize,
      this.tagConficence,
      this.isShowNumber,
      this.sizeOfScreen,
      this.color,
        this.background
      });

  ui.Image image;
  double tagSize;
  double tagConficence;
  List<Offset> hull;
  List<Offset> points;
  List<Circle> tags;
  bool isShowNumber;
  Size sizeOfScreen;
  Color color;
  Color background;
  final Paint blackBackground = new Paint()
    ..color = Colors.white
    ..style = PaintingStyle.fill;
  final Paint painter = new Paint()
    ..color = Colors.black.withOpacity(0.85)
    ..style = PaintingStyle.fill;

  final Paint circleBorder = Paint()
    ..strokeWidth = 1
    ..color = Colors.red.withOpacity(1)
    ..style = PaintingStyle.stroke;
  final Paint selectedCircle = Paint()
    ..strokeWidth = 3
    ..color = Colors.red.withOpacity(0.4)
    ..style = PaintingStyle.fill;
  final Paint maskPaint = Paint()
    ..blendMode = BlendMode.clear
    ..strokeWidth = 20
    ..strokeCap = StrokeCap.round;
  List<Circle> getTagInnerArea(List<Circle> tags) {
    if (hull.length == 0) return tags;
    List<Circle> result = List();
    for (int i = 0; i < tags.length; i++) {
      if (Hull.isInside(hull, Offset(tags[i].pos.dx, tags[i].pos.dy))) {
        result.add(tags[i]);
      }
    }
    return result;
  }

  void drawTextCenter(
      Canvas context, int num, double x, double y, double size) {
    num += 1;
    TextStyle style = TextStyle(
      color: color,
      fontSize: size,
      fontWeight: FontWeight.bold,
    );
    Size sizeOfText = getSizeOfText("$num", style);
    TextSpan span = new TextSpan(style: style, text: "$num");
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(context,
        new Offset(x - sizeOfText.width / 2, y - sizeOfText.height / 2));
  }

  void drawDate(
      Canvas context, String num, double maxsize, double y, double size) {
    TextStyle style = TextStyle(
      color: Colors.black,
      fontSize: size,
      fontWeight: FontWeight.bold,
    );
    Size sizeOfText = getSizeOfText("$num", style);
    TextSpan span = new TextSpan(style: style, text: "$num");
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(context, new Offset(maxsize - sizeOfText.width - 20, y));
  }

  void drawText(Canvas context, String str, double x, double y, double size) {
    TextStyle style = TextStyle(
      color: Colors.black,
      fontSize: size,
      fontWeight: FontWeight.bold,
    );
    TextSpan span = new TextSpan(style: style, text: "$str");
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(context, new Offset(x, y));
  }

  Size getSizeOfText(String text, TextStyle style) {
    final TextPainter textPainter = TextPainter(
        text: TextSpan(text: text, style: style),
        maxLines: 1,
        textDirection: TextDirection.ltr)
      ..layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.size;
  }

  void paintImage(
      ui.Image image, Rect outputRect, Canvas canvas, Paint paint, BoxFit fit) {
    final Size imageSize =
        Size(image.width.toDouble(), image.height.toDouble());
    final FittedSizes sizes = applyBoxFit(fit, imageSize, outputRect.size);
    final Rect inputSubrect =
        Alignment.center.inscribe(sizes.source, Offset.zero & imageSize);
    final Rect outputSubrect =
        Alignment.center.inscribe(sizes.destination, outputRect);
    canvas.drawImageRect(image, inputSubrect, outputSubrect, paint);
  }

  void drawHull(Canvas canvas, Paint mask) {
    if (hull.length > 0) {
      Path path = new Path();
      path.moveTo(hull[0].dx, hull[0].dy);
      for (int i = 0; i < hull.length - 1; i++) {
        if (hull[i] != null && hull[i + 1] != null) {
          // canvas.drawLine(points[i], points[i + 1], maskPaint);
          path.lineTo(hull[i].dx, hull[i].dy);
        }
      }
      path.close();
      canvas.drawPath(path, mask);
    }
  }

  @override
  void paint(Canvas canvas, Size size) {
    double width = sizeOfScreen.width;
    double height = sizeOfScreen.height;
    Rect borderRect = Rect.fromLTWH(0, 0, width, height);
    final Paint circleFill = Paint()
      ..strokeWidth = 3
      ..color = background
      ..style = PaintingStyle.fill;
    // paintImage(image, Rect.fromLTWH(0, 0, width, height), canvas, Paint(),
    //     BoxFit.contain);
    // canvas.drawRect(Rect.fromLTWH(0, 0, width, height), Paint()..color= Colors.blue);
    final df = new intl.DateFormat('dd-MM-yyyy hh:mm a');
    int myvalue = new DateTime.now().millisecondsSinceEpoch;
    var formattedDate =
        (df.format(new DateTime.fromMillisecondsSinceEpoch(myvalue)));
    canvas.drawImage(image, Offset.zero, Paint());
    Rect bottomRect = Rect.fromLTWH(0, height, width, 100);
    canvas.drawRect(bottomRect, blackBackground);
    drawText(canvas, "Kết quả: ${tags.length}", 20, height, tagSize * 1.5);
    drawDate(canvas, formattedDate, width, height, tagSize * 1.5);
    tags = getTagInnerArea(tags);

    for (int i = 0; i < tags.length; i++) {
      if (tags[i].confidence * 100 >= tagConficence) {
        Paint p = tags[i].isSelected ? selectedCircle : circleFill;
        canvas.drawCircle(tags[i].pos, tagSize, p);
        canvas.drawCircle(tags[i].pos, tagSize, circleBorder);
        if (isShowNumber)
          drawTextCenter(
              canvas, i, tags[i].pos.dx, tags[i].pos.dy, tagSize - 5);
      }
    }

    if (points.length > 0) {
      canvas.saveLayer(Rect.fromLTWH(0, 0, width, height), Paint());
      canvas.drawRect(Rect.fromLTWH(0, 0, width, height), painter);

      drawHull(canvas, maskPaint);
      for (int i = 0; i < points.length - 1; i++) {
        if (points[i] != null && points[i + 1] != null) {
          canvas.drawLine(points[i], points[i + 1], maskPaint);
        }
      }
      canvas.restore();
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class ImageEditor extends CustomPainter {
  ImageEditor(
      {this.image,
      this.points,
      this.hull,
      this.tags,
      this.tagSize,
      this.tagConficence,
      this.isShowNumber,
      this.sizeOfScreen,
      this.color,
        this.background = Colors.blue
      }) ;

  ui.Image image;
  double tagSize;
  double tagConficence;
  List<Offset> hull;
  List<Offset> points;
  List<Circle> tags;
  bool isShowNumber;
  Size sizeOfScreen;
  Color color;
  Color background;
  final Paint painter = new Paint()
    ..color = Colors.black.withOpacity(0.85)
    ..style = PaintingStyle.fill;

  final Paint circleBorder = Paint()
    ..strokeWidth = 1
    ..color = Colors.red.withOpacity(1)
    ..style = PaintingStyle.stroke;
  final Paint selectedCircle = Paint()
    ..strokeWidth = 3
    ..color = Colors.red.withOpacity(0.4)
    ..style = PaintingStyle.fill;
  final Paint maskPaint = Paint()
    ..blendMode = BlendMode.clear
    ..strokeWidth = 20
    ..strokeCap = StrokeCap.round;
  List<Circle> getTagInnerArea(List<Circle> tags) {
    if (hull.length == 0) return tags;
    List<Circle> result = List();
    for (int i = 0; i < tags.length; i++) {
      if (Hull.isInside(hull, Offset(tags[i].pos.dx, tags[i].pos.dy))) {
        result.add(tags[i]);
      }
    }
    return result;
  }

  void drawTextCenter(
      Canvas context, int num, double x, double y, double size) {
    num += 1;
    TextStyle style = TextStyle(
      color: color,
      fontSize: size,
      fontWeight: FontWeight.bold,
    );
    Size sizeOfText = getSizeOfText("$num", style);
    TextSpan span = new TextSpan(style: style, text: "$num");
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(context,
        new Offset(x - sizeOfText.width / 2, y - sizeOfText.height / 2));
  }

  Size getSizeOfText(String text, TextStyle style) {
    final TextPainter textPainter = TextPainter(
        text: TextSpan(text: text, style: style),
        maxLines: 1,
        textDirection: TextDirection.ltr)
      ..layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.size;
  }

  void paintImage(
      ui.Image image, Rect outputRect, Canvas canvas, Paint paint, BoxFit fit) {
    final Size imageSize =
        Size(image.width.toDouble(), image.height.toDouble());
    final FittedSizes sizes = applyBoxFit(fit, imageSize, outputRect.size);
    final Rect inputSubrect =
        Alignment.center.inscribe(sizes.source, Offset.zero & imageSize);
    final Rect outputSubrect =
        Alignment.center.inscribe(sizes.destination, outputRect);
    canvas.drawImageRect(image, inputSubrect, outputSubrect, paint);
  }

  void drawHull(Canvas canvas, Paint mask) {
    if (hull.length > 0) {
      Path path = new Path();
      path.moveTo(hull[0].dx, hull[0].dy);
      for (int i = 0; i < hull.length - 1; i++) {
        if (hull[i] != null && hull[i + 1] != null) {
          // canvas.drawLine(points[i], points[i + 1], maskPaint);
          path.lineTo(hull[i].dx, hull[i].dy);
        }
      }
      path.close();
      canvas.drawPath(path, mask);
    }
  }

  @override
  void paint(Canvas canvas, Size size) {
    double width = sizeOfScreen.width;
    double height = sizeOfScreen.height;
    Rect borderRect = Rect.fromLTWH(0, 0, width, height);
    final Paint circleFill = Paint()
      ..strokeWidth = 3
      ..color = background
      ..style = PaintingStyle.fill;
    // paintImage(image, Rect.fromLTWH(0, 0, width, height), canvas, Paint(),
    //     BoxFit.contain);
    // canvas.drawRect(Rect.fromLTWH(0, 0, width, height), Paint()..color= Colors.blue);
    canvas.drawImage(image, Offset.zero, Paint());
    tags = getTagInnerArea(tags);
    for (int i = 0; i < tags.length; i++) {
      if (tags[i].confidence * 100 >= tagConficence) {
        Paint p = tags[i].isSelected ? selectedCircle : circleFill;
        canvas.drawCircle(tags[i].pos, tagSize, p);
        canvas.drawCircle(tags[i].pos, tagSize, circleBorder);
        if (isShowNumber)
          drawTextCenter(
              canvas, i, tags[i].pos.dx, tags[i].pos.dy, tagSize - 5);
      }
    }
    if (points.length > 0) {
      canvas.saveLayer(Rect.fromLTWH(0, 0, width, height), Paint());
      canvas.drawRect(Rect.fromLTWH(0, 0, width, height), painter);

      drawHull(canvas, maskPaint);
      for (int i = 0; i < points.length - 1; i++) {
        if (points[i] != null && points[i + 1] != null) {
          canvas.drawLine(points[i], points[i + 1], maskPaint);
        }
      }
      canvas.restore();
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
