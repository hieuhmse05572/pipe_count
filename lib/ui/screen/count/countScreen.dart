import 'dart:io';

import 'package:flutter/material.dart';

class CountScreen extends StatefulWidget {
  final String imagePath;
  CountScreen({this.imagePath});
  @override
  _CountScreenState createState() => _CountScreenState();
}

class NavObject {
  Widget screen;
  Icon navIcon;
  Text title;
  NavObject({this.screen, this.navIcon, this.title});
}

List<NavObject> navItems = [
  NavObject(
    navIcon: Icon(Icons.format_shapes),
    title: Text('Area'),
  ),
  NavObject(
    navIcon: Icon(Icons.touch_app),
    title: Text('Tags'),
  ),
  NavObject(
    navIcon: Icon(
      Icons.all_out,
      color: Colors.red,
    ),
    title: Text(
      'Count',
      style: TextStyle(fontSize: 20),
    ),
  ),
  NavObject(
    navIcon: Icon(Icons.all_inclusive),
    title: Text('Hide'),
  ),
  NavObject(
    navIcon: Icon(Icons.menu),
    title: Text('Menu'),
  ),
];

class _CountScreenState extends State<CountScreen> {
  int _screenNumber = 0;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final deviceRatio = size.width / size.height;
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        color: Colors.blue,
        child: Transform.scale(
            scale: 1,
            child: Center(
              child: AspectRatio(
                aspectRatio: 0.75,
                child: Image.file(
                  File(widget.imagePath),
                ),
              ),
            )),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: navItems
            .map((navItem) => BottomNavigationBarItem(
                  icon: navItem.navIcon,
                  title: navItem.title,
                ))
            .toList(),
        currentIndex: _screenNumber,
        onTap: (i) => setState(() {
          _screenNumber = i;
        }),
      ),
    );
  }
}
