import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:pipeCount/getIt.dart';
import 'package:pipeCount/src/controllers/counter_screen_controller.dart';
import 'package:pipeCount/src/utils/sharedPref.dart';
import 'package:pipeCount/ui/screen/camera/slider.dart';
import 'package:pipeCount/ui/screen/settings/colorScreen.dart';
import 'package:pipeCount/ui/screen/settings/language.dart';
import 'package:pipeCount/ui/screen/settings/result_image.dart';
import 'package:pipeCount/ui/widgets/dialog.dart';
import 'package:pipeCount/ui/widgets/line.dart';
import 'package:pipeCount/ui/widgets/switch.dart';

class Settings extends StatelessWidget {
  final TextStyle titleStyle = TextStyle(color: Colors.white, fontSize: 13);
  final menuProvider = getIt<MenuProvider>();

  Widget text(BuildContext context, String key) {
    String txt = FlutterI18n.translate(context, key);
    return Text(txt, style: titleStyle);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        menuProvider.changeSetting(false);
        Navigator.pop(context);
      },
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Card(
            margin: EdgeInsets.all(0),
            color: Colors.transparent,
            child: buildSettings(context)),
      ),
    );
  }

  Widget buildSettings(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(flex: 1, child: buildMenu(context)),
        Expanded(
          flex: 7,
          child: Container(
              // height: 500,
              child: Column(
            children: [
              buildTagSetting(context),
              // Divider(
              //   thickness: 7,
              //   height: 7,
              //   // color: Colors.white.withOpacity(0.5),
              // ),
              buildSaveSetting(context)
            ],
          )),
        ),
      ],
    );
  }

  Widget buildMenu(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 30, left: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          IconButton(
            onPressed: () {
              menuProvider.changeSetting(false);
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_rounded,
              color: Colors.white,
            ),
          ),
          Text(
            FlutterI18n.translate(context, "title.settings"),
            style: TextStyle(color: Colors.white, fontSize: 17),
          ),
          Text(
            "",
            style: TextStyle(color: Colors.white, fontSize: 17),
          ),

          // RaisedButton(
          //   elevation: 0,
          //   splashColor: Colors.pink,
          //   color: Colors.transparent,
          //   onPressed: () {
          //     menuProvider.ScreenShot();
          //   },
          //   child: text(context, "title.save"),
          // )
        ],
      ),
    );
  }

  Widget buildTagSetting(BuildContext context) {
    final menuProvider = getIt<MenuProvider>();

    return Card(
      color: Colors.transparent,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            title: Text(
              'TAGS',
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 15,
                  fontWeight: FontWeight.normal),
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.pop(context);
              DialogUtil.showDialogSettingRight(context, ColorScreen());
            },
            child: ListTile(
              leading: Icon(
                Icons.color_lens_outlined,
                color: Colors.white,
              ),
              title: text(context, "title.tag_colors"),
              trailing: Icon(Icons.keyboard_arrow_right, color: Colors.white),
            ),
          ),
          Line(
            axis: Axis.horizontal,
          ),
          InkWell(
            onTap: () {},
            child: ListTile(
                leading: Icon(
                  Icons.visibility_outlined,
                  color: Colors.white,
                ),
                title: text(
                  context,
                  'title.show_tag_number',
                ),
                trailing: CustomSwitch(
                  initSwitchValue:
                      SharePref.prefs.getBool('tags.show_tag_number') ?? false,
                  onTap: (value) {
                    SharePref.prefs.setBool('tags.show_tag_number', value);
                    menuProvider.isShowNumber = value;
                    menuProvider.refresh();
                  },
                )),
          ),
          Line(
            axis: Axis.horizontal,
          ),
          ListTile(
            subtitle: Padding(
              padding: const EdgeInsets.only(top: 10, left: 5),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(
                    Icons.circle,
                    color: Colors.white,
                    size: 20,
                  ),
                  Container(
                    width: 0.7 * MediaQuery.of(context).size.width,
                    child: CustomSlider(
                      type: "",
                      initValue:
                          (SharePref.prefs.getDouble('tags.default_tag_size') ??
                                  50)
                              .toInt(),
                      onChange: (double newValue) {
                        SharePref.prefs
                            .setDouble('tags.default_tag_size', newValue);
                      },
                    ),
                  ),
                  Icon(
                    Icons.circle,
                    color: Colors.white,
                    size: 40,
                  ),
                ],
              ),
            ),
            title: text(
              context,
              "title.default_tag_size",
            ),
          ),
          Line(
            axis: Axis.horizontal,
          ),
          InkWell(
            onTap: () {
              Navigator.pop(context);
              DialogUtil.showDialogSettingRight(context, LanguageScreen());
            },
            child: ListTile(
              leading: Icon(
                Icons.language_outlined,
                color: Colors.white,
              ),
              title: text(
                context,
                'title.language',
              ),
              subtitle: Text(
                FlutterI18n.currentLocale(context).languageCode,
                style: TextStyle(color: Colors.pink),
              ),
              trailing: Icon(Icons.keyboard_arrow_right, color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildSaveSetting(BuildContext context) {
    final menuProvider = getIt<MenuProvider>();

    return Card(
      color: Colors.transparent,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            title: Text(
              'SAVE',
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 15,
                  fontWeight: FontWeight.normal),
            ),
          ),
          // InkWell(
          //   onTap: () {
          //     Navigator.pop(context);
          //     DialogUtil.showDialogSettingRight(context, ResultImageScreen());
          //   },
          //   child: ListTile(
          //     leading: Icon(
          //       Icons.broken_image,
          //       color: Colors.white,
          //     ),
          //     title: text(context, "title.result_image"),
          //     trailing: Icon(Icons.keyboard_arrow_right, color: Colors.white),
          //   ),
          // ),
          // Line(
          //   axis: Axis.horizontal,
          // ),
          InkWell(
            onTap: () {},
            child: ListTile(
                leading: Icon(
                  Icons.image_outlined,
                  color: Colors.white,
                ),
                title: text(context, "title.save_original_photo"),
                trailing: CustomSwitch(
                  initSwitchValue:
                      SharePref.prefs.getBool('tags.save_original_photo') ??
                          false,
                  onTap: (value) {
                    SharePref.prefs.setBool('tags.save_original_photo', value);
                    menuProvider.isSaveOriginalImage = value;
                  },
                )),
          ),
          Line(
            axis: Axis.horizontal,
          ),
        ],
      ),
    );
  }
}
