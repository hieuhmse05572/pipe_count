import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:pipeCount/getIt.dart';
import 'package:pipeCount/src/controllers/counter_screen_controller.dart';
import 'package:pipeCount/src/utils/constants.dart';
import 'package:pipeCount/src/utils/i18n.dart';
import 'package:pipeCount/src/utils/sharedPref.dart';
import 'package:pipeCount/ui/screen/settings/settings.dart';
import 'package:pipeCount/ui/widgets/dialog.dart';
import 'package:pipeCount/ui/widgets/line.dart';
import 'package:pipeCount/ui/widgets/switch.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ColorScreen extends StatelessWidget {
  final menuProvider = getIt<MenuProvider>();
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context);
        DialogUtil.showDialogSettingLeft(context, Settings());
      },
      child: Scaffold(
          backgroundColor: Colors.transparent, body: buildSettings(context)),
    );
  }

  Widget buildSettings(BuildContext context) {
    // create some values
    Color background = menuProvider.background;
    Color color = menuProvider.background;

// ValueChanged<Color> callback
    void changeColor(Color color) {}

// raise the [showDialog] widget
    void showColorPicker(Color color, int type) {
      Color change;

      showDialog(
        context: context,
        child: AlertDialog(
          title: const Text(
            'Pick a color!',
          ),
          content: SingleChildScrollView(
            child: ColorPicker(
              pickerColor: color,
              onColorChanged: (Color value) {
                change = value;
              },
              showLabel: true,
              pickerAreaHeightPercent: 0.8,
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: const Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: const Text('Default'),
              onPressed: () {
                if (type == 0) {
                  menuProvider.background = Colors.blue.withOpacity(0.7);
                  SharePref.prefs.setInt(
                      'tags.background', Colors.blue.withOpacity(0.7).value);
                } else {
                  menuProvider.color = Colors.white;
                  SharePref.prefs.setInt('tags.color', Colors.white.value);
                }
                Navigator.of(context).pop();
                menuProvider.refresh();
              },
            ),
            FlatButton(
              child: const Text('Got it'),
              onPressed: () {
                if (type == 0) {
                  menuProvider.background = change;
                  SharePref.prefs.setInt('tags.background', change.value);
                } else {
                  menuProvider.color = change;
                  SharePref.prefs.setInt('tags.color', change.value);
                }
                Navigator.of(context).pop();
                menuProvider.refresh();
              },
            ),
          ],
        ),
      );
    }

    return Column(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(
            padding: EdgeInsets.only(top: 30, left: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                    DialogUtil.showDialogSettingLeft(context, Settings());
                  },
                  icon: Icon(
                    Icons.arrow_back_rounded,
                    color: Colors.white,
                  ),
                ),
                Text(
                  // text(context, "title.language"),
                  "Color Picker",
                  style: TextStyle(color: Colors.white, fontSize: 17),
                ),
                Text(' ')
              ],
            ),
          ),
        ),
        Expanded(
          flex: 7,
          child: Card(
            margin: EdgeInsets.all(0),
            color: Colors.transparent,
            child: Container(
              color: Colors.transparent,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    title: Text(
                      'COLORS',
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 15,
                          fontWeight: FontWeight.normal),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      showColorPicker(background, 0);
                    },
                    child: ListTile(
                      leading: Icon(
                        Icons.color_lens_outlined,
                        color: Colors.white,
                      ),
                      title: Text(
                        'Background',
                        style: TextStyle(color: Colors.white),
                      ),
                      trailing:
                          Icon(Icons.keyboard_arrow_right, color: Colors.white),
                    ),
                  ),
                  Line(
                    axis: Axis.horizontal,
                  ),
                  InkWell(
                      onTap: () {
                        showColorPicker(color, 1);
                      },
                      child: ListTile(
                          leading: Icon(
                            Icons.visibility_outlined,
                            color: Colors.white,
                          ),
                          title: Text(
                            'Color',
                            style: TextStyle(color: Colors.white),
                          ),
                          trailing: Icon(Icons.keyboard_arrow_right,
                              color: Colors.white))),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
