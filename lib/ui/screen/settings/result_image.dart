import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:pipeCount/src/utils/constants.dart';
import 'package:pipeCount/src/utils/sharedPref.dart';
import 'package:pipeCount/ui/screen/settings/settings.dart';
import 'package:pipeCount/ui/widgets/dialog.dart';
import 'package:pipeCount/ui/widgets/line.dart';
import 'package:pipeCount/ui/widgets/switch.dart';

class ResultImageScreen extends StatelessWidget {
  final TextStyle titleStyle = TextStyle(color: Colors.white, fontSize: 13);
  Widget text(BuildContext context, String key) {
    String txt = FlutterI18n.translate(context, key);
    return Text(txt, style: titleStyle);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent, body: buildSettings(context));
  }

  Widget buildSettings(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(
            padding: EdgeInsets.only(top: 30, left: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                    DialogUtil.showDialogSettingLeft(context, Settings());
                  },
                  icon: Icon(
                    Icons.arrow_back_rounded,
                    color: Colors.white,
                  ),
                ),
                Text(
                  FlutterI18n.translate(context, "title.result_image"),
                  style: TextStyle(color: Colors.white, fontSize: 17),
                ),
                Text(' ')
              ],
            ),
          ),
        ),
        Expanded(
          flex: 7,
          child: Container(
              color: Colors.transparent, child: buildTagSetting(context)),
        ),
      ],
    );
  }

  Widget buildTagSetting(BuildContext context) {
    return Card(
      color: Colors.transparent,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            title: Text(
              FlutterI18n.translate(context, "title.result_image_settings"),
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 15,
                  fontWeight: FontWeight.normal),
            ),
          ),
          InkWell(
            onTap: () {},
            child: ListTile(
                leading: Icon(
                  Icons.visibility_outlined,
                  color: Colors.white,
                ),
                title: text(
                  context,
                  'title.show_tags_numbers',
                ),
                trailing: CustomSwitch(
                  initSwitchValue:
                      SharePref.prefs.getBool('image.show_tags_numbers') ??
                          false,
                  onTap: (value) {
                    SharePref.prefs.setBool('image.show_tags_numbers', value);
                  },
                )),
          ),
          Line(
            axis: Axis.horizontal,
          ),
          InkWell(
            onTap: () {},
            child: ListTile(
                leading: Icon(
                  Icons.panorama_wide_angle_rounded,
                  color: Colors.white,
                ),
                title: text(
                  context,
                  'title.show_count_area',
                ),
                trailing: CustomSwitch(
                  initSwitchValue:
                      SharePref.prefs.getBool('image.show_count_area') ?? false,
                  onTap: (value) {
                    SharePref.prefs.setBool('image.show_count_area', value);
                  },
                )),
          ),
          Line(
            axis: Axis.horizontal,
          ),
          InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: ListTile(
              leading: Icon(
                Icons.date_range_outlined,
                color: Colors.white,
              ),
              title: text(
                context,
                'title.show_date',
              ),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'Th 4, tháng 11 11, 2020',
                    style: TextStyle(color: Colors.white, fontSize: 12),
                  ),
                  Icon(Icons.keyboard_arrow_right, color: Colors.white),
                ],
              ),
            ),
          ),
          Line(
            axis: Axis.horizontal,
          ),
          InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: ListTile(
              leading: Icon(
                Icons.access_time_outlined,
                color: Colors.white,
              ),
              title: text(
                context,
                'title.show_time',
              ),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    '14:19',
                    style: TextStyle(color: Colors.white, fontSize: 12),
                  ),
                  Icon(Icons.keyboard_arrow_right, color: Colors.white),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
