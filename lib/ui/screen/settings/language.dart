import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:pipeCount/getIt.dart';
import 'package:pipeCount/src/controllers/counter_screen_controller.dart';
import 'package:pipeCount/src/utils/constants.dart';
import 'package:pipeCount/src/utils/i18n.dart';
import 'package:pipeCount/src/utils/sharedPref.dart';
import 'package:pipeCount/ui/screen/settings/settings.dart';
import 'package:pipeCount/ui/widgets/dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LanguageScreen extends StatelessWidget {
  final menuProvider = getIt<MenuProvider>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent, body: buildSettings(context));
  }

  Widget buildSettings(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(
            padding: EdgeInsets.only(top: 30, left: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                    DialogUtil.showDialogSettingLeft(context, Settings());
                  },
                  icon: Icon(
                    Icons.arrow_back_rounded,
                    color: Colors.white,
                  ),
                ),
                Text(
                  text(context, "title.language"),
                  style: TextStyle(color: Colors.white, fontSize: 17),
                ),
                Text(' ')
              ],
            ),
          ),
        ),
        Expanded(
          flex: 7,
          child: Card(
            margin: EdgeInsets.all(0),
            color: Colors.transparent,
            child: Container(
              color: Colors.transparent,
              child: ListView.separated(
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    onTap: () async {
                      String lang = Language.languages[index].locate;
                      SharePref.prefs.setString("lang", lang);
                      await FlutterI18n.refresh(context, Locale(lang));
                      menuProvider.languageRefresh();
                      Navigator.pop(context);
                      DialogUtil.showDialogSettingLeft(context, Settings());
                    },
                    title: Text(
                      '${Language.languages[index].name}',
                      style: TextStyle(color: Colors.white),
                    ),
                  );
                },
                itemCount: Language.languages.length,
                separatorBuilder: (BuildContext context, int index) {
                  return Divider(
                    color: Colors.grey,
                  );
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}
