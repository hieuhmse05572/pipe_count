import 'package:camera/new/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:get/get.dart';
import 'package:pipeCount/i18n.dart';
import 'package:pipeCount/ui/screen/splash/splash.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class PipeCount extends StatelessWidget {
  FlutterI18nDelegate flutterI18nDelegate;
  PipeCount(this.flutterI18nDelegate);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
        primaryColor: Colors.lightBlue,
        primarySwatch: Colors.lightBlue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Splash(),

      localizationsDelegates: [
        flutterI18nDelegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      builder: FlutterI18n.rootAppBuilder(),
      // home: CaptureScreen(),
      // initialRoute: '/splash',
    );
  }
}
