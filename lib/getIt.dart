import 'package:get_it/get_it.dart';
import 'package:pipeCount/src/controllers/counter_screen_controller.dart';

final getIt = GetIt.instance;

void setup() {
  getIt.registerSingleton<MenuProvider>(MenuProvider());
}
