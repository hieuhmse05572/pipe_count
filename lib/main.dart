import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:pipeCount/i18n.dart';
import 'package:pipeCount/pipeCount.dart';
import 'package:pipeCount/ui/screen/camera/camera.dart';

import 'getIt.dart';

Future<void> main() async {
  setup();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  FlutterI18nDelegate flutterI18nDelegate = await initI18n();
  runApp(PipeCount(flutterI18nDelegate));
}
//
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
//
// void main() => runApp(MyApp());
//
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Persistent Bottom Navigation Bar example project',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: MainMenu(),
//       initialRoute: '/',
//     );
//   }
// }
//
// class MainMenu extends StatefulWidget {
//   MainMenu({Key key}) : super(key: key);
//
//   @override
//   _MainMenuState createState() => _MainMenuState();
// }
//
// class _MainMenuState extends State<MainMenu> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Sample Project"),
//       ),
//       body: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: <Widget>[
//           SizedBox(height: 20.0),
//           Center(
//             child: RaisedButton(
//               child: Text("Built-in styles example"),
//               onPressed: () => pushNewScreen(
//                 context,
//                 screen: ProvidedStylesExample(
//                   menuScreenContext: context,
//                 ),
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
//
// // ----------------------------------------- Provided Style ----------------------------------------- //
//
// class ProvidedStylesExample extends StatefulWidget {
//   final BuildContext menuScreenContext;
//   ProvidedStylesExample({Key key, this.menuScreenContext}) : super(key: key);
//
//   @override
//   _ProvidedStylesExampleState createState() => _ProvidedStylesExampleState();
// }
//
// class _ProvidedStylesExampleState extends State<ProvidedStylesExample> {
//   PersistentTabController _controller;
//   bool _hideNavBar;
//
//   @override
//   void initState() {
//     super.initState();
//     _controller = PersistentTabController(initialIndex: 0);
//     _hideNavBar = false;
//   }
//
//   List<Widget> _buildScreens() {
//     return [
//       Container(color: Colors.blue,),
//       Container(),
//       Container(),
//       Container(),
//       Container(),
//     ];
//   }
//
//   List<PersistentBottomNavBarItem> _navBarsItems() {
//     return [
//       PersistentBottomNavBarItem(
//         icon: Icon(Icons.format_shapes),
//         title: "Area",
//         activeColor: Colors.blue,
//         inactiveColor: Colors.grey,
//       ),
//       PersistentBottomNavBarItem(
//         icon: Icon(Icons.touch_app),
//         title: ("Tags"),
//         activeColor: Colors.teal,
//         inactiveColor: Colors.grey,
//       ),
//       PersistentBottomNavBarItem(
//         icon: Icon(Icons.all_out),
//         title: ("Count"),
//         activeColor: Colors.blueAccent,
//         inactiveColor: Colors.grey,
//         activeContentColor: Colors.white,
//       ),
//       PersistentBottomNavBarItem(
//         icon: Icon(Icons.confirmation_number),
//         title: ("Hide"),
//         activeColor: Colors.deepOrange,
//         inactiveColor: Colors.grey,
//       ),
//       PersistentBottomNavBarItem(
//         icon: Icon(Icons.settings),
//         title: ("Menu"),
//         activeColor: Colors.indigo,
//         inactiveColor: Colors.grey,
//       ),
//     ];
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: const Text('')),
//       drawer: Drawer(
//         child: Center(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               const Text('This is the Drawer'),
//             ],
//           ),
//         ),
//       ),
//       body: PersistentTabView(
//         controller: _controller,
//         screens: _buildScreens(),
//         items: _navBarsItems(),
//         confineInSafeArea: true,
//         backgroundColor: Colors.white,
//         handleAndroidBackButtonPress: true,
//         resizeToAvoidBottomInset: true,
//         stateManagement: true,
//         hideNavigationBarWhenKeyboardShows: true,
//         hideNavigationBar: _hideNavBar,
//         margin: EdgeInsets.all(10.0),
//         popActionScreens: PopActionScreensType.once,
//         bottomScreenMargin: 0.0,
//         // onWillPop: () async {
//         //   await showDialog(
//         //     context: context,
//         //     useSafeArea: true,
//         //     builder: (context) => Container(
//         //       height: 50.0,
//         //       width: 50.0,
//         //       color: Colors.white,
//         //       child: RaisedButton(
//         //         child: Text("Close"),
//         //         onPressed: () {
//         //           Navigator.pop(context);
//         //         },
//         //       ),
//         //     ),
//         //   );
//         //   return false;
//         // },
//         decoration: NavBarDecoration(
//             colorBehindNavBar: Colors.indigo,
//             borderRadius: BorderRadius.circular(20.0)),
//         popAllScreensOnTapOfSelectedTab: true,
//         itemAnimationProperties: ItemAnimationProperties(
//           duration: Duration(milliseconds: 400),
//           curve: Curves.ease,
//         ),
//         screenTransitionAnimation: ScreenTransitionAnimation(
//           animateTabTransition: true,
//           curve: Curves.ease,
//           duration: Duration(milliseconds: 200),
//         ),
//         navBarStyle:
//             NavBarStyle.style15, // Choose the nav bar style with this property
//       ),
//     );
//   }
// }
//
// // ----------------------------------------- Custom Style ----------------------------------------- //
//
// class CustomNavBarWidget extends StatelessWidget {
//   final int selectedIndex;
//   final List<PersistentBottomNavBarItem> items;
//   final ValueChanged<int> onItemSelected;
//
//   CustomNavBarWidget({
//     Key key,
//     this.selectedIndex,
//     @required this.items,
//     this.onItemSelected,
//   });
//
//   Widget _buildItem(PersistentBottomNavBarItem item, bool isSelected) {
//     return Container(
//       alignment: Alignment.center,
//       height: kBottomNavigationBarHeight,
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         crossAxisAlignment: CrossAxisAlignment.center,
//         mainAxisSize: MainAxisSize.min,
//         children: <Widget>[
//           Flexible(
//             child: IconTheme(
//               data: IconThemeData(
//                   size: 26.0,
//                   color: isSelected
//                       ? (item.activeContentColor == null
//                           ? item.activeColor
//                           : item.activeContentColor)
//                       : item.inactiveColor == null
//                           ? item.activeColor
//                           : item.inactiveColor),
//               child: item.icon,
//             ),
//           ),
//           Padding(
//             padding: const EdgeInsets.only(top: 5.0),
//             child: Material(
//               type: MaterialType.transparency,
//               child: FittedBox(
//                   child: Text(
//                 item.title,
//                 style: TextStyle(
//                     color: isSelected
//                         ? (item.activeContentColor == null
//                             ? item.activeColor
//                             : item.activeContentColor)
//                         : item.inactiveColor,
//                     fontWeight: FontWeight.w400,
//                     fontSize: item.titleFontSize),
//               )),
//             ),
//           )
//         ],
//       ),
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       color: Colors.white,
//       child: Container(
//         width: double.infinity,
//         height: kBottomNavigationBarHeight,
//         child: Row(
//           mainAxisAlignment: MainAxisAlignment.spaceAround,
//           children: items.map((item) {
//             var index = items.indexOf(item);
//             return Flexible(
//               child: GestureDetector(
//                 onTap: () {
//                   this.onItemSelected(index);
//                 },
//                 child: _buildItem(item, selectedIndex == index),
//               ),
//             );
//           }).toList(),
//         ),
//       ),
//     );
//   }
// }
