import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

Future<FlutterI18nDelegate> initI18n() async {
  final FlutterI18nDelegate flutterI18nDelegate = FlutterI18nDelegate(
    translationLoader: FileTranslationLoader(
        useCountryCode: false,
        fallbackFile: 'en',
        basePath: 'assets/i18n',
        forcedLocale: Locale('en')),
  );
  WidgetsFlutterBinding.ensureInitialized();
  await flutterI18nDelegate.load(null);
  return flutterI18nDelegate;
}
